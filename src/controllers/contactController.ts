import { Request, Response } from "express"
import WebhookModel, { createWebhook } from "../models/webhook";
import Webhook from "../classes/webhook";
import mongoose from "mongoose";
import { getWebhookReference } from "../helpers/blip/utils";
import { listContacts, updateContactsFromBlip } from "../models/contact";
import { getMigrationToRunByInstance } from "../models/migration";
import { getInstanceById } from "../models/instance";
import { calculateTimeDifferenceInMinutes, calculateTimeDifferenceInSeconds } from "../helpers/util";

class WebhookController {
    static webhookController: WebhookController;
    static getInstance = function () {
        if (WebhookController.webhookController == undefined) {
            WebhookController.webhookController = new WebhookController();
        }
        return WebhookController.webhookController;
    };

    async list(req: Request | any, res?: Response) {
        try {
            let { instance, query } = req
            let { search } = query

            let result = await listContacts({
                instance: instance._id,
                search
            })

            res?.status(200).json({
                ...result,
                success: true
            })
        } catch (error: any) {
            console.error(error)
            res?.status(error?.status || 500).json(error)
        }
    }

    async update(req: Request | any, res?: Response) {
        try {
            let { instance: instanceId } = req.params
            let { force = false } = req.query

            force = [true, "true"].includes(force)

            let { instance } = await getInstanceById(instanceId)

            if (!instance)
                throw { message: "Instância não encontrada" }

            let finished = true;
            do {
                let { migration } = await getMigrationToRunByInstance({ instance: instanceId, type: "contact" })
                console.log("migration", migration)
                let lastUpdate = instance?.lastUpdate?.contact

                if (migration?.status == "processing" && (!migration.updatedAt || calculateTimeDifferenceInSeconds(migration.updatedAt, new Date()) < 15))
                    throw { success: false, message: "No momento há uma migração em andamento" }

                if (!migration && !force) {
                    if (lastUpdate && calculateTimeDifferenceInMinutes(lastUpdate, new Date()) < (60 * 1))
                        throw { success: false, message: "Fora do período de atualização" }
                }

                let { finished: f } = await updateContactsFromBlip({
                    instance: instance._id,
                    lastUpdate: instance?.lastUpdate?.contact,
                    migration
                })
                finished = f
            } while (finished === false)

            res?.status(200).json({
                success: true
            })
        } catch (error: any) {
            console.error(error)
            res?.status(error?.status || 500).json(error)
        }
    }
}

export default WebhookController.getInstance();