'use strict';
import 'dotenv/config';
import { Request, Response } from "express";
import InstanceModel, { createInstance, getInstanceById, updateInstance } from '../models/instance';
import { authorizationDecode, checkConnectivity, setWebhook } from '../helpers/blip';
import { defaultTakeContact } from '../models/contact';
import MigrationModel, { createMigration } from '../models/migration';
import mongoose from 'mongoose';

const { API_URL_PROD } = process.env

class InstanceController {
    static instanceController: InstanceController;
    static getInstance = function () {
        if (InstanceController.instanceController == undefined) {
            InstanceController.instanceController = new InstanceController();
        }
        return InstanceController.instanceController;
    };

    async get(req: Request | any, res: Response) {
        try {
            // console.log(req.headers)
            let { language } = req.headers
            let Authorization = req.headers.Authorization || req.headers.authorization
            let tenantId = req.headers.tenantId || req.headers.tenantid

            if (!Authorization) throw { status: 404, message: "Authorization não informado" }
            if (!tenantId) throw { status: 404, message: "tenantId não informado" }

            let instance: any = await InstanceModel
                .findOne({ client_tenant: tenantId, token: Authorization, deleted_at: undefined })
                .populate({ path: "plan", select: "_id name contacts_limit messages_limit" })
                .lean()

            if (!instance || instance?.status == 0) throw { status: 404, message: "Instância não encontrada", notFound: true }
            if (instance?.status == 2) throw { status: 401, message: "Instância sem permissão" }

            if (instance.token != Authorization)
                throw {
                    status: 200,
                    success: false,
                    messageHTML: `Esta instância só pode ser acessada via <b>${instance.client_short_name}</b>`,
                    message: `Esta instância só pode ser acessada via ${instance.client_short_name}`,
                    otherBot: true,
                    bot: instance.client_short_name
                }

            instance.webhook = `${API_URL_PROD}/webhook/${instance._id}`

            instance.migration = await MigrationModel
                .findOne({
                    instance: instance._id,
                    status: { $in: ["waiting", "processing"] }
                })

            res.status(200).json({
                success: true,
                instance
            })
        } catch (error: any) {
            console.error(error)
            res.status(error?.status || 500).json(error)
        }
    }

    async post(req: Request | any, res: Response) {
        try {
            // console.log(req.headers)
            let { language } = req.headers
            let Authorization = req.headers.Authorization || req.headers.authorization
            let tenantId = req.headers.tenantId || req.headers.tenantid
            let { plan } = req.body
            // let {
            //     execution_token,
            //     service = "middleware_for_blip"
            // } = req.body

            let execution_token = Authorization

            if (!Authorization) throw { status: 404, message: "Authorization não informado" }
            if (!tenantId) throw { status: 404, message: "tenantId não informado" }
            if (!plan) throw { stats: 404, message: "Plano não informado" }

            let check = await checkConnectivity({ token: Authorization, tenantId })
            if (!check?.status || check?.status != "success")
                throw { status: 401, message: "Instância inválida" }


            check = await checkConnectivity({ token: execution_token, tenantId })
            if (!check?.status || check?.status != "success")
                throw { status: 401, message: "Roteador ou bot inválido" }

            let { shortName, accessKey } = authorizationDecode(Authorization)

            let instanceCreated: any = await createInstance({
                execution_token,
                token: Authorization,
                client_tenant: tenantId,
                client_access_key: accessKey,
                client_short_name: shortName,
                plan: new mongoose.Types.ObjectId(plan)
            })

            await setWebhook({ token: Authorization, tenantId }, true, `${API_URL_PROD}/webhook/${instanceCreated._id}`)

            let { migration } = await createMigration({
                instance: instanceCreated._id,
                type: "contact",
                take: defaultTakeContact,
                nextMigration: "chat"
            })

            let { instance } = await getInstanceById(instanceCreated._id)

            if (!instance || instance?.status == 0) throw { status: 404, message: "Instância não encontrada", notFound: true }
            if (instance?.status == 2) throw { status: 401, message: "Instância sem permissão" }

            instance.migration = migration

            res.status(200).json({
                success: true,
                data: instance
            })
        } catch (error: any) {
            console.error(error)
            res.status(error?.status || 500).json(error)
        }
    }

    async put(req: Request | any, res: Response) {
        try {
            let { language } = req.headers
            let Authorization = req.headers.Authorization || req.headers.authorization
            let { instance } = req.decoded
            let { execution_token } = req.body

            const check = await checkConnectivity(Authorization)
            if (!check?.status || check?.status != "success")
                throw { status: 401, message: "Instância inválida" }

            if (!instance || instance?.status == 0) throw { status: 404, message: "Instância não encontrada", notFound: true }
            if (instance?.status == 2) throw { status: 401, message: "Instância sem permissão" }

            instance = await updateInstance(instance._id, {
                execution_token
            })

            res.status(200).json({
                success: true,
                data: instance
            })
        } catch (error: any) {
            console.error(error)
            res.status(error?.status || 500).json(error)
        }
    }

    async updateWebhook(req: Request | any, res: Response) {
        try {
            let Authorization = req.headers.Authorization || req.headers.authorization
            let { instance } = req

            await setWebhook({
                token: Authorization,
                tenantId: instance.client_tenant
            }, true, `${API_URL_PROD}/webhook/${instance._id}`)

            res.status(200).json({
                success: true
            })
        } catch (error: any) {
            console.error(error)
            res.status(error?.status || 500).json(error)
        }
    }
}

export default InstanceController.getInstance();