import { Request, Response } from "express"
import { getContactByIdentity, listContacts } from "../models/contact";
import WebhookModel from "../models/webhook";
import mongoose from "mongoose";
import ChatModel, { updateChatFromBlip, updateChatFromBlipByIdentity } from "../models/chat";
import { getMigrationToRunByInstance } from "../models/migration";
import { getInstanceById } from "../models/instance";
import { calculateTimeDifferenceInMinutes, calculateTimeDifferenceInSeconds } from "../helpers/util";

class ChatController {
    static chatController: ChatController;
    static getInstance = function () {
        if (ChatController.chatController == undefined) {
            ChatController.chatController = new ChatController();
        }
        return ChatController.chatController;
    };

    async get(req: Request | any, res?: Response) {
        try {
            let { identity } = req.params
            let { instance } = req

            let { contact } = await getContactByIdentity({ instance: instance._id, identity }) || {}

            if (contact)
                await updateChatFromBlipByIdentity({
                    identity,
                    lastSync: contact.lastSync,
                    instance: instance._id,
                    token: instance.token,
                    tenantId: instance.client_tenant
                })

            let where = {
                instance: new mongoose.Types.ObjectId(instance._id),
                "reference.type": { $in: ["message", "attendanceRedirect"] },
                "reference.identity": identity
            }

            console.log(where)

            let chat = await ChatModel
                .find(where)
                .select("-instance")
                .sort("reference.datetime")

            res?.status(200).json({
                chat,
                contact: contact?.contact,
                success: true
            })
        } catch (error: any) {
            console.error(error)
            res?.status(error?.status || 500).json(error)
        }
    }

    async update(req: Request | any, res?: Response) {
        try {
            let { instance: instanceId } = req.params
            let { force = false } = req.query

            force = [true, "true"].includes(force)

            let { instance } = await getInstanceById(instanceId)

            if (!instance)
                throw { message: "Instância não encontrada" }

            let finished = true;
            do {
                let { migration } = await getMigrationToRunByInstance({ instance: instanceId, type: "chat" })

                let lastUpdate = instance?.lastUpdate?.chat

                if (migration?.status == "processing" && (!migration.updatedAt || calculateTimeDifferenceInSeconds(migration.updatedAt, new Date()) < 15))
                    throw { success: false, message: "No momento há uma migração em andamento" }

                if (!migration && !force) {
                    if (lastUpdate && calculateTimeDifferenceInMinutes(lastUpdate, new Date()) < (60 * 24))
                        throw { success: false, message: "Fora do período de atualização" }
                }

                let { finished: f } = await updateChatFromBlip({
                    instance: instance._id,
                    migration
                })
                finished = f
            } while (finished === false)

            res?.status(200).json({
                success: true
            })
        } catch (error: any) {
            console.error(error?.response?.data)
            res?.status(error?.status || 500).json(error)
        }
    }
}

export default ChatController.getInstance();