import axios from "axios";
import { Request, Response } from "express"
import { getInstances } from "../models/instance";
import { executeExternalContactUpdate } from "../models/contact";
import { executeExternalChatUpdate } from "../models/chat";
const { API_URL } = process.env

class CRONController {
    static webhookController: CRONController;
    static getInstance = function () {
        if (CRONController.webhookController == undefined) {
            CRONController.webhookController = new CRONController();
        }
        return CRONController.webhookController;
    };

    async routineUpdatedChats(req: Request | any, res?: Response) {
        try {
            let { instances = [] } = await getInstances()

            await Promise.all(
                instances.map(
                    (instance) => executeExternalChatUpdate({ instance: instance._id })
                )
            )

            res?.status(200).json({
                success: true
            })
        } catch (error: any) {
            res?.status(error?.status || 500).json(error)
        }
    }

    async routineUpdatedContacts(req: Request | any, res?: Response) {
        try {
            let { instances = [] } = await getInstances()

            await Promise.all(
                instances.map(
                    (instance) => executeExternalContactUpdate({ instance: instance._id })
                )
            )

            res?.status(200).json({
                success: true
            })
        } catch (error: any) {
            res?.status(error?.status || 500).json(error)
        }
    }
}

export default CRONController.getInstance();