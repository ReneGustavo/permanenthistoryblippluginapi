import { Request, Response } from "express"
import WebhookModel, { createWebhook } from "../models/webhook";
import Webhook from "../classes/webhook";
import mongoose from "mongoose";
import { getWebhookReference } from "../helpers/blip/utils";

class WebhookController {
    static webhookController: WebhookController;
    static getInstance = function () {
        if (WebhookController.webhookController == undefined) {
            WebhookController.webhookController = new WebhookController();
        }
        return WebhookController.webhookController;
    };

    async post(req: Request | any, res?: Response) {
        try {
            let { body, query, params } = req

            let { instance_id } = params

            await createWebhook({ instance: instance_id, body, query })

            res?.status(200).json({
                success: true
            })
        } catch (error: any) {
            console.error(error)
            res?.status(error?.status || 500).json(error)
        }
    }
}

export default WebhookController.getInstance();