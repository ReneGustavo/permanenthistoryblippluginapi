export type MigrationTypes = "contact" | "chat";
export type MigrationStatus = "pending" | "processing" | "finished" | "canceled";