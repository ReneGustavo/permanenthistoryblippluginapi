export type WebhookTypes = "message" | "attendanceRedirect" | "contact"

export type WebhookReference = {
    id?: string,
    type?: WebhookTypes,
    origin?: "blip" | "user",
    identity?: string,
    datetime?: Date
}