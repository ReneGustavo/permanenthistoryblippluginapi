import jwt from 'jsonwebtoken'

export const generateJWT = async (
    json: { [key: string]: any },
    expirar?: any,
    secret = process.env.SECRET || 'secret'
): Promise<string> => {

    let options: any = {}
    expirar && (options['expiresIn'] = expirar)

    return jwt.sign(json, secret, options)

}

export const verifyJWT = async (
    token: string,
    secret = process.env.SECRET || 'secret'
): Promise<any> => {
    if (!secret)
        throw { status: 400, message: 'Secret not found' }
    if (!token)
        throw { status: 401, message: 'Token not found' }

    return jwt.verify(token, secret, function (err: any, decoded: any) {
        if (err)
            throw { status: 401, message: 'Invalid token', logout: true }
        return decoded
    })

}