export const formatSecondsToTime = (seconds: number, addSeconds: boolean = true): string => {
    const hours = Math.floor(seconds / 3600);
    const minutes = Math.floor((seconds % 3600) / 60);
    const remainingSeconds = Math.floor(seconds % 60);

    const formattedHours = hours.toString().padStart(2, '0');
    const formattedMinutes = minutes.toString().padStart(2, '0');
    const formattedSeconds = remainingSeconds.toString().padStart(2, '0');

    return `${formattedHours}:${formattedMinutes}${addSeconds ? `:${formattedSeconds}` : ""}`;
}

export const formatCPFCNPJ = (value: string): string => {
    // Remove non-numeric characters
    const digitsOnly = value.replace(/\D/g, '');

    // Check if it is a CPF or CNPJ
    if (digitsOnly.length === 11) {
        // Format CPF
        return digitsOnly.replace(/(\d{3})(\d{3})(\d{3})(\d{2})/, '$1.$2.$3-$4');
    } else if (digitsOnly.length === 14) {
        // Format CNPJ
        return digitsOnly.replace(/(\d{2})(\d{3})(\d{3})(\d{4})(\d{2})/, '$1.$2.$3/$4-$5');
    } else {
        // If it's neither CPF nor CNPJ, return the original value
        return value;
    }
}

export const removeUndefinedValues = (obj: any): any => {
    if (typeof obj !== 'object' || obj === null) {
        return obj;
    }

    if (Array.isArray(obj)) {
        return obj.map((item) => removeUndefinedValues(item));
    }

    const result: any = {};

    for (const key in obj) {
        if (obj.hasOwnProperty(key)) {
            const value = removeUndefinedValues(obj[key]);
            if (value !== undefined) {
                result[key] = value;
            }
        }
    }

    return result;
}

export const extractNumbers = (inputString: string): string => {
    // Use a regular expression to match and extract numbers
    const numbersOnly = inputString.replace(/\D/g, '');

    return numbersOnly;
}