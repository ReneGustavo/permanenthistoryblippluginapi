export const generateRandomPassword = (digits = 8) => {
    let caracteres = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789'

    let password = ''
    for (let i = 0; i < digits; i++) {
        password += caracteres[exports.generageRandomNumber(0, caracteres.length - 1)] + ''
    }

    return password
}

export const generageRandomNumber = (min = 0, max = 1) => {
    return parseInt((Math.random() * (max - min) + min).toString())
}

export const generateCode = (digits = 4) => {
    if (digits < 1) digits = 1
    return exports.generageRandomNumber(Math.pow(10, digits - 1), Math.pow(10, digits) - 1)
}