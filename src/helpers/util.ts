import { join as path_join } from 'path';

import { Moment } from 'moment';
import mongoose, { mongo } from 'mongoose';

export const calculateTimeDifferenceInSeconds = (date1: Date, date2: Date): number => {
    const timeDifferenceInMilliseconds = Math.abs(date2.getTime() - date1.getTime());
    const timeDifferenceInMinutes = timeDifferenceInMilliseconds / 1000;

    return timeDifferenceInMinutes;
}
export const calculateTimeDifferenceInMinutes = (date1: Date, date2: Date): number => {
    const timeDifferenceInMilliseconds = Math.abs(date2.getTime() - date1.getTime());
    const timeDifferenceInMinutes = timeDifferenceInMilliseconds / (60 * 1000);

    return timeDifferenceInMinutes;
}
export const calculateTimeDifferenceInDays = (date1: Date, date2: Date, absolute: boolean = true): number => {
    let result = Math.floor((date1.getTime() - date2.getTime()) / (24 * 60 * 60 * 1000))
    return absolute ? Math.abs(result) : result
}

export const getValueByPath = (object: { [key: string]: any }, path: string) => {
    if (!object)
        return null

    const keys = path.split('.');
    let value: any = object;

    for (const key of keys) {
        if (value && typeof value === 'object' && key in value) {
            value = value[key];
        } else {
            return null;
        }
    }

    return value;
}

export const hasPermission = (permissions: any, permission_keys: string | string[], and: boolean = true) => {
    if (!permissions)
        return false

    for (let permission_key of (Array.isArray(permission_keys) ? permission_keys : [permission_keys])) {
        if (!getValueByPath(permissions, permission_key))
            return false
    }

    return true
}

export const addMinutesToDate = (date: Date, minutes: number): Date => {
    const novaData = new Date(date.getTime() + minutes * 60000); // 60000 milissegundos em 1 minuto
    return novaData;
}
export const mergeObjects = (obj1: any, obj2: any) => {
    if (typeof obj1 !== 'object' || typeof obj2 !== 'object') {
        return obj2;
    }

    const result = { ...obj1 };

    for (const key in obj2) {
        if (obj2.hasOwnProperty(key)) {
            result[key] = mergeObjects(obj1[key], obj2[key]);
        }
    }

    return result;
}


export const addPeriod = (date: Date | Moment, amount: number, unit: 'year' | 'month' | 'week' | 'day' | 'hour' | 'minute' | 'second' | 'millisecond') => {
    if (date instanceof Date) {
        const newDate = new Date(date);

        switch (unit) {
            case 'year':
                newDate.setFullYear(newDate.getFullYear() + amount);
                break;
            case 'month':
                newDate.setMonth(newDate.getMonth() + amount);
                break;
            case 'week':
                newDate.setDate(newDate.getDate() + amount * 7);
                break;
            case 'day':
                newDate.setDate(newDate.getDate() + amount);
                break;
            case 'hour':
                newDate.setHours(newDate.getHours() + amount);
                break;
            case 'minute':
                newDate.setMinutes(newDate.getMinutes() + amount);
                break;
            case 'second':
                newDate.setSeconds(newDate.getSeconds() + amount);
                break;
            case 'millisecond':
                newDate.setMilliseconds(newDate.getMilliseconds() + amount);
                break;
            default:
                throw new Error('Unrecognized unit: ' + unit);
        }

        return newDate;
    } else if (typeof date === 'object' && typeof date.add === 'function') {
        return date.clone().add(amount, unit);
    } else {
        throw new Error('Unsupported date type');
    }
}

export const removeFalseFromObject = (obj: any): any => {
    if (Array.isArray(obj)) {
        // Se for um array, filtre os elementos que não sejam "false"
        const newArray = obj.filter((item) => item !== false);
        // Chame a função recursivamente para cada elemento do novo array
        return newArray.map((item) => removeFalseFromObject(item));
    } else if (typeof obj === "object" && obj !== null) {
        // Se for um objeto, filtre as propriedades cujo valor não seja "false"
        const newObj: { [key: string]: any } = {};
        for (const key in obj) {
            if (obj.hasOwnProperty(key) && obj[key] !== false) {
                newObj[key] = removeFalseFromObject(obj[key]);
            }
        }
        return newObj;
    } else {
        // Caso contrário, retorne o valor sem modificação
        return obj;
    }
}

export const joinDirectory = (...args: any[]) => {
    let args2: string[] = []

    for (let arg of args)
        args2 = [...args2, ...arg.split("/")]

    return path_join(process.cwd(), ...args2)
}

export const isEqual = (_ids: mongoose.Types.ObjectId[] | string[]) => {
    for (let [index, _id] of _ids.entries()) {
        if (index > 0 && !(new mongoose.Types.ObjectId(_id)).equals(new mongoose.Types.ObjectId(_ids?.[0])))
            return false
    }

    return true
}

export const PromiseAllInChunks = async<T>(promises: Promise<T>[], chunkSize: number): Promise<T[]> => {
    const results: T[] = [];

    // Função auxiliar para processar um chunk de promises
    const processChunk = async (chunk: Promise<T>[]) => {
        for (const promise of chunk) {
            try {
                const result = await promise;
                results.push(result);
            } catch (error) {
                // Trate os erros conforme necessário
                console.error(`Error in promise: ${error}`);
            }
        }
    };

    // Divide as promises em chunks
    for (let i = 0; i < promises.length; i += chunkSize) {
        const chunk = promises.slice(i, i + chunkSize);
        await processChunk(chunk);
    }

    return results;
}

export const isCurrentDateBetween = (startDate: Date | string, endDate: Date | string, dateToCompare: Date | string = new Date()): boolean => {
    startDate = new Date(startDate)
    endDate = new Date(endDate)
    dateToCompare = new Date(dateToCompare)

    return dateToCompare >= startDate && dateToCompare <= endDate
}

export const generatePossiblePhones = (phone: string) => {
    phone = phone.replace(/\D/g, '')

    let versions: any[] = [
        { useDDI: false, useNinth: true },
        { useDDI: false, useNinth: false },
        { useDDI: true, useNinth: true },
        { useDDI: true, useNinth: false },
    ]

    let phones = versions.map(({ useDDI, useNinth }: any) => {
        let { ddi = "55", ddd, number }: any = {}

        if (phone.length == 10) { // Sem DDI e sem 9
            ddd = phone.substring(0, 2)
            number = phone.slice(2)
        } else if (phone.length == 11) { // Sem DDI e com 9
            ddd = phone.substring(0, 2)
            number = phone.slice(3)
        } else if (phone.length == 12) { // Com DDI e sem 9
            ddi = phone.substring(0, 2)
            ddd = phone.substring(2, 4)
            number = phone.slice(4)
        } else if (phone.length == 13) { // Com DDI e com 9
            ddi = phone.substring(0, 2)
            ddd = phone.substring(2, 4)
            number = phone.slice(5)
        }

        let newPhone = ""

        if (useDDI) newPhone += ddi
        newPhone += ddd
        if (useNinth) newPhone += "9"
        newPhone += number

        return newPhone
    })

    return phones
}

export const btoa = (content: string) => {
    return Buffer.from(content).toString('base64')
}
export const atob = (b64Encoded: string) => {
    return Buffer.from(b64Encoded, 'base64').toString()
}