import { runCommand } from "."
import { defaultTakeContact } from "../../models/contact"

export const getContacts = async ({ token, tenantId }: { token: string, tenantId: string }, {
    skip = 0,
    take = defaultTakeContact, // Entre 1 e 30000
    filter
}: {
    skip?: number,
    take?: number,
    filter?: any
}) => {
    try {
        let queries = [
            ["$skip", skip],
            ["$take", take],
            ["$filter", filter]
        ].filter(([key, value]) => value != undefined)

        console.log(queries)

        let { data } = await runCommand({
            token,
            tenantId,
            to: "postmaster@crm.msging.net",
            method: "get",
            uri: `/contacts?${queries.map(([key, value]) => `${key}=${value}`).join("&")}`
        })

        console.log("dataaa", data)

        let { resource } = data

        return resource
    } catch (error) {
        throw error
    }
}