import { WebhookReference, WebhookTypes } from "../../types/webhook.types";

export const getWebhookType = (obj: { [key: string]: any }): WebhookTypes | undefined => {
    try {
        if (typeof obj != "object")
            return undefined;

        if (obj?.content) {
            let { content } = obj

            if (typeof content == "string")
                return "message"
            if (content.team)
                return "attendanceRedirect"
            return "message"
        }

        if (obj?.lastMessageDate && obj?.identity) {
            return "contact"
        }
    } catch (error) {
        throw error
    }
}

export const getWebhookReference = (obj: { [key: string]: any }): WebhookReference => {
    try {
        let type = getWebhookType(obj)

        let { from, id, to, direction, date, metadata } = obj

        let datetimeReference = date || +metadata?.date_processed || +metadata?.date_created
        let idReference = id || metadata?.$internalId

        let reference: WebhookReference = {
            type,
            datetime: datetimeReference ? new Date(datetimeReference) : undefined,
            id: idReference
        }

        if (from && to) {
            let origin: "blip" | "user" = from.indexOf("@msging.net") > 0 ? "blip" : "user"
            let identity = origin == "blip" ? to?.split("/")?.[0] : from?.split("/")?.[0]

            reference.origin = origin
            reference.identity = identity
        } else if (direction) {
            let origin: "blip" | "user" = direction === "sent" ? "blip" : "user"

            reference.origin = origin
        }

        return reference
    } catch (error) {
        throw error
    }
}