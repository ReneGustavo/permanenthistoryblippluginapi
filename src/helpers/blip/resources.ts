import { runCommand } from "."

export const getResource = async ({ token, tenantId }: { token: string, tenantId: string }, {
    key
}: {
    key: string
}) => {
    try {
        let { data } = await runCommand({
            token,
            tenantId,
            to: "postmaster@msging.net",
            method: "get",
            uri: `/resources/${key}`
        })

        return data.resource
    } catch (error) {
        throw error
    }
}

export const setResource = async ({ token, tenantId }: { token: string, tenantId: string }, {
    type,
    key,
    resource
}: {
    type: string,
    key: string,
    resource: any
}) => {
    try {
        let { data } = await runCommand({
            token,
            tenantId,
            to: "postmaster@msging.net",
            method: "set",
            uri: `/resources/${key}`,
            type,
            resource
        })

        return data
    } catch (error) {
        throw error
    }
}