import { runCommand } from "."

export const verifyIdentity = async ({ token, tenantId }: { token: string, tenantId: string }, phoneNumber: string) => {
    try {
        let { data } = await runCommand({
            token,
            tenantId,
            to: "postmaster@wa.gw.msging.net",
            method: "get",
            uri: `lime://wa.gw.msging.net/accounts/${phoneNumber}`
        })

        let { status, resource, reason } = data

        if (status == "failure")
            throw { code: reason?.code, message: reason?.description }

        return resource
    } catch (error) {
        throw error
    }
}

export const sendActiveMessage = async ({ token, tenantId }: { token: string, tenantId: string }, {
    phoneNumber,
    identity,
    type = "template",
    namespace,
    template_name,
    language = { code: "pt_BR", policy: "deterministic" },
    components
}: {
    phoneNumber?: string,
    identity?: string,
    type?: string,
    namespace: string,
    template_name: string,
    language?: { code: string, policy: string },
    components?: any[]
}) => {
    try {
        if (phoneNumber && !identity) {
            let resource = await verifyIdentity({ token, tenantId }, phoneNumber)

            if (!resource)
                throw { status: 400, message: `Destinatário desconhecido` }

            identity = resource.identity
        }

        let result = await runCommand({
            token,
            tenantId,
            path: "messages",
            to: identity as any,
            type: "application/json",
            method: "post",
            content: {
                type,
                template: {
                    namespace,
                    name: template_name,
                    language,
                    components
                }
            }
        })

        let { data } = result

        if (data == "")
            data = null

        return data
    } catch (error) {
        throw error
    }
}

export const getMessagesByIdentity = async ({ token, tenantId }: { token: string, tenantId: string }, {
    identity,
    take = 100, // Entre 1 e 100
    refreshExpiredMedia = true,
    storageDate
}: {
    identity: string,
    take?: number,
    refreshExpiredMedia?: boolean,
    storageDate?: Date
}) => {
    try {
        let queries = [
            ["$take", take],
            ["refreshExpiredMedia", refreshExpiredMedia ? "true" : "false"],
            ["storageDate", storageDate ? storageDate.toISOString() : undefined]
        ].filter(([key, value]) => value != undefined)

        identity = identity?.trim()

        let { data } = await runCommand({
            token,
            tenantId,
            method: "get",
            uri: `/threads/${identity}?${queries.map(([key, value]) => `${key}=${value}`).join("&")}`
        })

        // console.log(data)

        let { resource } = data

        return resource
    } catch (error) {
        throw error
    }
}