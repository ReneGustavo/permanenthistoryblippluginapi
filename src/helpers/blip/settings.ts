import { runCommand } from "."
import { defaultTakeContact } from "../../models/contact"

export const getWebhook = async ({ token, tenantId }: { token: string, tenantId: string }) => {
    try {
        let { data } = await runCommand({
            token,
            tenantId,
            uri: "lime://postmaster@analytics.msging.net/configuration",
            method: "get",
            type: "application/json",
            resource: {}
        })

        return data
    } catch (error) {
        throw error
    }
}

export const setWebhook = async ({ token, tenantId }: { token: string, tenantId: string }, {
    isValid,
    url,
    replaceUrl = false
}: {
    isValid?: boolean,
    url?: string,
    replaceUrl?: boolean
}) => {
    try {
        let obj: { [key: string]: any } = {}

        let webhook = await getWebhook({ token, tenantId })

        if (isValid != undefined)
            obj["Webhook.isValid"] = isValid ? "True" : "False"

        if (url)
            obj["Webhook.Url"] = replaceUrl ? url : [...(webhook["Webhook.Url"] || "").split(";"), url].join(";")

        let { data } = await runCommand({
            token,
            tenantId,
            uri: "lime://postmaster@analytics.msging.net/configuration",
            method: "set",
            type: "application/json",
            resource: obj
        })

        let { resource } = data

        return resource
    } catch (error) {
        throw error
    }
}