import { atob, btoa } from "../util"
import apiBlip from "./api"
import axios from "axios"

const { PROXY = "" } = process.env

export const authorizationDecode = (token: string) => {
    token = token.replace("Key ", "")

    let [shortName, accessKeyEncoded] = atob(token)?.split(":")

    let accessKey = btoa(accessKeyEncoded)

    return { shortName, accessKey }
}

export const authorizationEncode = (shortName: string, accessKey: string) => {
    return `Key ${btoa(`${shortName}:${atob(accessKey)}`)}`
}

export const runCommand = async ({
    token,
    tenantId,
    action = "blip_api",
    path = "commands",
    id = "{{$guid}}",
    to,
    method,
    uri,
    http,
    ...rest
}: {
    token: string,
    tenantId: string,
    [key: string]: any
}
) => {
    try {
        let result;

        console.log("runCommand", action, path, http)

        if (action == "blip_api") {
            result = await apiBlip.post(`${PROXY}https://${tenantId}.http.msging.net/${path}`, {
                id,
                to,
                method,
                uri,
                ...rest
            }, {
                headers: {
                    "Content-Type": "application/json",
                    Authorization: token
                }
            })
        } else if (action == "http") {
            console.log("HTTP REQ", http)
            result = await axios(http)
        } else
            throw { message: "Action não reconhecido" }

        return result
    } catch (error) {
        // console.error(error)
        throw error
    }
}

export const checkConnectivity = async ({ token, tenantId }: { token: string, tenantId: string }) => {
    try {
        let { data } = await runCommand({
            token,
            tenantId,
            to: "postmaster@msging.net",
            method: "get",
            uri: "/ping"
        })

        return data
    } catch (error) {
        throw error
    }
}

export const setResource = async ({ token, tenantId }: { token: string, tenantId: string }, {
    type,
    key,
    resource
}: {
    type: string,
    key: string,
    resource: any
}) => {
    try {
        let { data } = await runCommand({
            token,
            tenantId,
            to: "postmaster@msging.net",
            method: "set",
            uri: `/resources/${key}`,
            type,
            resource
        })

        return data
    } catch (error) {
        throw error
    }
}

export const setAdvancedConfiguration = async ({ token, tenantId }: { token: string, tenantId: string }, resource: object) => {
    try {
        let { data } = await runCommand({
            token,
            tenantId,
            method: "set",
            uri: "lime://postmaster@analytics.msging.net/configuration",
            type: "application/json",
            resource
        })

        return data
    } catch (error) {
        throw error
    }
}

export const getAdvancedConfiguration = async ({ token, tenantId }: { token: string, tenantId: string }) => {
    try {
        let { data } = await runCommand({
            token,
            tenantId,
            method: "get",
            uri: "lime://postmaster@analytics.msging.net/configuration",
            resource: {}
        })

        console.log(data)

        if (data?.status != "success")
            throw { status: 400 }

        let { resource } = data

        return resource
    } catch (error) {
        throw error
    }
}

export const setWebhook = async ({ token, tenantId }: { token: string, tenantId: string }, isValid: boolean, url: string | Array<string>, replace: boolean = false) => {
    try {
        let urls = Array.isArray(url) ? url : [url]

        if (!replace) {
            let resource: any = {};
            try {
                resource = await getAdvancedConfiguration({ token, tenantId })
            } catch { }

            if (resource["Webhook.Url"]) {
                let urls2 = resource["Webhook.Url"].split(";").map((url: string) => url.trim())

                for (let url of urls) {
                    if (!urls2.includes(url))
                        urls2.push(url)
                }

                urls = urls2
            }
        }

        return await setAdvancedConfiguration({ token, tenantId }, {
            "Webhook.IsValid": isValid ? "True" : "False",
            "Webhook.Url": urls.map(url => url.trim()).join(";")
        })
    } catch (error) {
        throw error
    }
}