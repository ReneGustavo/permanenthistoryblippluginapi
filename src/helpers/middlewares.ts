/* eslint-disable no-prototype-builtins */
import 'dotenv/config'
import type { Request, Response, NextFunction } from 'express'
import { verifyJWT } from './jwt'
import { getInstanceByToken } from '../models/instance'

class MiddlewaresController {
    static middlewaresController: MiddlewaresController
    static getInstance = function () {
        if (MiddlewaresController.middlewaresController == undefined) {
            MiddlewaresController.middlewaresController = new MiddlewaresController()
        }
        return MiddlewaresController.middlewaresController
    }

    async basicAuth(req: Request | any, res: Response, next: NextFunction | undefined) {
        try {
            let token;
            for (let key of ["authorization", "Authorization"]) {
                if (req?.headers?.[key]) {
                    token = req.headers[key]
                    break
                }
            }

            token = token.replace("Bearer ", "")

            if (!token) throw { status: 401, message: 'Authorization Token não encontrado', logout: true }
            if (token != process.env.AUTH_TOKEN && token != process.env.CRON_SECRET) throw { status: 401, message: 'Authorization Token sem permissão', logout: true }

            if (next) next()
        } catch (error: any) {
            return res.status(error?.status || 500).json(error)
        }
    }

    async middlewareInstance(req: Request | any, res: Response, next: NextFunction | undefined) {
        try {
            let token;
            for (let key of ["authorization", "Authorization"]) {
                if (req?.headers?.[key]) {
                    token = req.headers[key]
                    break
                }
            }

            if (!token) throw { status: 401, message: 'Authorization Token não encontrado', logout: true }

            let { instance } = await getInstanceByToken(token)

            if (!instance)
                throw { status: 404, message: "Instância não encontrada" }

            req.instance = instance

            if (next) next()
        } catch (error: any) {
            return res.status(error?.status || 500).json(error)
        }
    }
}

export default MiddlewaresController.getInstance()
