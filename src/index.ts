import mongoose from 'mongoose';
import app from './app';
import InstanceModel, { updateComsumption } from './models/instance';
import ChatModel from './models/chat';
import ContactModel from './models/contact';

const PORT = process.env.API_PORT || 3333;

app.listen(PORT, async () => {
  /* eslint-disable no-console */
  console.info(`Server ready and listening on ${PORT}`);
  /* eslint-enable no-console */
});

// (async () => {
//   console.log("Iniciar atualização de consumo")
//   let instances = await InstanceModel.find({})

//   for (let instance of instances) {
//     let result = await updateComsumption(instance._id)
//     console.log(instance.client_tenant, result)
//   }
// })()