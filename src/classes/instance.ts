import mongoose from 'mongoose';

export default class Instance {
  public _id: mongoose.Types.ObjectId;
  public created_at: Date;
  public updated_at?: Date;
  public deleted_at?: Date;
  public status: number;

  public execution_token: string;
  public token: string;

  public client_tenant: string;
  public client_access_key: string;
  public client_short_name: string;

  public plan: mongoose.Types.ObjectId;
  public contacts_consumption: number;
  public messages_consumption: number;

  public lastUpdate?: {
    contact?: Date;
    chat?: Date;
  };

  public migration?: any;

  constructor(instance: IInstance) {
    this._id = instance._id;
    this.created_at = instance.created_at;
    this.updated_at = instance.updated_at;
    this.deleted_at = instance.deleted_at;
    this.status = instance.status;

    this.execution_token = instance.execution_token;
    this.token = instance.token;

    this.client_tenant = instance.client_tenant;
    this.client_access_key = instance.client_access_key;
    this.client_short_name = instance.client_short_name;

    this.plan = instance.plan;
    this.contacts_consumption = instance.contacts_consumption;
    this.messages_consumption = instance.messages_consumption;

    this.lastUpdate = instance.lastUpdate;

    this.migration = instance.migration;
  }
}

export interface IInstance {
  _id: mongoose.Types.ObjectId,
  created_at: Date;
  updated_at?: Date;
  deleted_at?: Date;
  status: number;

  execution_token: string
  token: string;

  client_tenant: string;
  client_access_key: string;
  client_short_name: string;

  plan: mongoose.Types.ObjectId;
  contacts_consumption: number;
  messages_consumption: number;

  lastUpdate?: {
    contact?: Date;
    chat?: Date;
  };

  migration?: any;
}