import mongoose from 'mongoose';

export default class Contact {
    public _id: mongoose.Types.ObjectId;
    public createdAt: Date;
    public updatedAt?: Date;

    public instance: mongoose.Types.ObjectId;

    public contact: any;
    public lastSync?: Date;

    constructor(contact: IContact) {
        this._id = contact._id;
        this.createdAt = contact.createdAt;
        this.updatedAt = contact.updatedAt;

        this.instance = contact.instance;

        this.contact = contact.contact;
        this.lastSync = contact.lastSync;
    }
}

export interface IContact {
    _id: mongoose.Types.ObjectId;
    createdAt: Date;
    updatedAt?: Date;

    instance: mongoose.Types.ObjectId;

    contact: any;
    lastSync?: Date;
}
