import mongoose from 'mongoose';
import { MigrationStatus, MigrationTypes } from '../types/migration.types';

export default class Migration {
    public _id: mongoose.Types.ObjectId;
    public createdAt: Date;
    public updatedAt?: Date;

    public instance: mongoose.Types.ObjectId;

    public status: MigrationStatus;

    public type?: MigrationTypes;
    public filter?: { [key: string]: any };
    public page?: number;
    public take?: number;
    public totalSize?: number;

    public nextMigration?: MigrationTypes;

    public reference?: {
        type?: string;
        origin?: string;
        identity?: string;
    };

    constructor(migration: IMigration) {
        this._id = migration._id;
        this.createdAt = migration.createdAt;
        this.updatedAt = migration.updatedAt;

        this.instance = migration.instance;

        this.status = migration.status;

        this.type = migration.type;
        this.filter = migration.filter;
        this.page = migration.page;
        this.take = migration.take;
        this.totalSize = migration.totalSize;

        this.nextMigration = migration.nextMigration;
    }
}

export interface IMigration {
    _id: mongoose.Types.ObjectId;
    createdAt: Date;
    updatedAt?: Date;

    instance: mongoose.Types.ObjectId;

    status: MigrationStatus;

    type?: MigrationTypes;
    filter?: { [key: string]: any };
    page?: number;
    take?: number;
    totalSize?: number;

    nextMigration?: MigrationTypes;
}