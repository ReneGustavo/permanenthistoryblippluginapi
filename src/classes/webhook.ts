import mongoose from 'mongoose';

export default class Webhook {
    public _id: mongoose.Types.ObjectId;
    public createdAt: Date;
    public updatedAt?: Date;

    public instance: mongoose.Types.ObjectId;

    public body?: object;
    public query?: object;

    public reference?: {
        type?: string;
        origin?: string;
        identity?: string;
    };

    constructor(webhook: IWebhook) {
        this._id = webhook._id;
        this.createdAt = webhook.createdAt;
        this.updatedAt = webhook.updatedAt;

        this.instance = webhook.instance;

        this.body = webhook.body;
        this.query = webhook.query;

        this.reference = webhook.reference;
    }
}

export interface IWebhook {
    _id: mongoose.Types.ObjectId;
    createdAt: Date;
    updatedAt?: Date;

    instance: mongoose.Types.ObjectId;

    body?: object;
    query?: object;
    reference?: {
        type?: string;
        origin?: string;
        identity?: string;
    };
}