import mongoose from 'mongoose';

export default class Chat {
    public _id: mongoose.Types.ObjectId;
    public createdAt: Date;
    public updatedAt?: Date;

    public instance: mongoose.Types.ObjectId;

    public body?: object;
    public query?: object;

    public reference?: {
        type?: string;
        origin?: string;
        identity?: string;
    };

    constructor(chat: IChat) {
        this._id = chat._id;
        this.createdAt = chat.createdAt;
        this.updatedAt = chat.updatedAt;

        this.instance = chat.instance;

        this.body = chat.body;
        this.query = chat.query;

        this.reference = chat.reference;
    }
}

export interface IChat {
    _id: mongoose.Types.ObjectId;
    createdAt: Date;
    updatedAt?: Date;

    instance: mongoose.Types.ObjectId;

    body?: object;
    query?: object;
    reference?: {
        id?: string;
        type?: string;
        origin?: string;
        identity?: string;
        datetime?: Date;
    };
}