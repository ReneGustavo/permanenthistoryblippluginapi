import mongoose from 'mongoose';

export default class Plan {
  public _id: mongoose.Types.ObjectId;
  public createdAt: Date;
  public updatedAt?: Date;
  public deletedAt?: Date;
  public status: number;

  public name: string;

  public contacts_limit: number;
  public messages_limit: number;

  constructor(plan: IPlan) {
    this._id = plan._id;
    this.createdAt = plan.createdAt;
    this.updatedAt = plan.updatedAt;
    this.deletedAt = plan.deletedAt;
    this.status = plan.status;

    this.name = plan.name;

    this.contacts_limit = plan.contacts_limit;
    this.messages_limit = plan.messages_limit;
  }
}

export interface IPlan {
  _id: mongoose.Types.ObjectId,
  createdAt: Date;
  updatedAt?: Date;
  deletedAt?: Date;
  status: number;

  name: string

  contacts_limit: number;
  messages_limit: number;
}