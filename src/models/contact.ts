import mongoose, { Schema, model } from 'mongoose';
import Contact, { IContact } from '../classes/contact';
import WebhookModel from './webhook';
import { getContacts } from '../helpers/blip/contacts';
import { getInstanceById, updateInstance } from './instance';
import { updateChatFromBlipByIdentity } from './chat';
import moment from 'moment';
import { IMigration } from '../classes/migration';
import MigrationModel, { createMigration, updateMigration } from './migration';
import axios from 'axios';

const { API_URL } = process.env

export const defaultTakeContact = 100

// SCHEMA

export const ContactSchema = new Schema({
    _id: {
        type: mongoose.Schema.Types.ObjectId,
        required: true,
    },
    createdAt: {
        type: Date,
        required: true,
    },
    updatedAt: {
        type: Date,
        required: false,
    },

    instance: {
        type: mongoose.Schema.Types.ObjectId,
        required: true
    },

    contact: {
        type: Object,
        required: false
    },
    lastSync: {
        type: Date,
        required: false
    }
});

const ContactModel = model<IContact>('Contact', ContactSchema);
export default ContactModel;

export const listContacts = async ({
    instance,
    search
}: {
    instance: string | mongoose.Types.ObjectId,
    search?: string
}) => {
    try {
        // let aggregate = await WebhookModel.aggregate([
        //     {
        //         $match: {
        //             instance: new mongoose.Types.ObjectId(instance),
        //             "reference.type": "message"
        //         },
        //     },
        //     {
        //         $sort: {
        //             createdAt: -1,
        //         },
        //     },
        //     {
        //         $group: {
        //             _id: "$reference.identity",
        //         },
        //     },
        //     {
        //         $lookup: {
        //             from: "contacts",
        //             localField: "_id",
        //             foreignField: "contact.identity",
        //             as: "contact",
        //         },
        //     },
        //     {
        //         $unwind: {
        //             path: "$contact",
        //             includeArrayIndex: "string",
        //             preserveNullAndEmptyArrays: true,
        //         },
        //     },
        //     {
        //         $project: {
        //             _id: 0,
        //             identity: "$_id",
        //             contact: "$contact.contact",
        //         },
        //     },
        // ])

        let where: any = {
            instance: new mongoose.Types.ObjectId(instance)
        }
        if (search)
            where.$or = [
                { "contact.name": { $regex: `.*${search}.*`, $options: "i" } },
                { "contact.phoneNumber": { $regex: `.*${search}.*`, $options: "i" } },
                { "contact.identity": { $regex: `.*${search}.*`, $options: "i" } },
                { "contact.document": { $regex: `.*${search}.*`, $options: "i" } },
                { "contact.gender": { $regex: `.*${search}.*`, $options: "i" } },
                { "contact.city": { $regex: `.*${search}.*`, $options: "i" } },
            ]

        console.log(JSON.stringify(where))

        let contacts = await ContactModel.find(where)

        return { contacts }
    } catch (error) {
        throw error
    }
}

export const getContactByIdentity = async ({
    instance,
    identity
}: {
    instance: mongoose.Types.ObjectId,
    identity: string
}) => {
    try {
        let contact = await ContactModel.findOne({ instance, "contact.identity": identity, deletedAt: undefined })

        return { contact }
    } catch (error) {
        throw error
    }
}

export const createContact = async ({
    instance,
    contact
}: {
    instance: string | mongoose.Types.ObjectId,
    contact: any
}) => {
    try {
        let { identity } = contact

        if (!identity) throw { message: "Identity não informado" }

        let cont = new Contact({
            _id: new mongoose.Types.ObjectId(),
            createdAt: new Date(),

            instance: new mongoose.Types.ObjectId(instance),

            contact
        })

        let contactCreated = await ContactModel.create(cont)

        return { contact: contactCreated }
    } catch (error) {
        throw error
    }
}

export const updateOrCreateContact = async ({
    instance,
    contact
}: {
    instance: string | mongoose.Types.ObjectId,
    contact: any
}) => {
    try {
        let { identity } = contact


        let newContact: any;

        let contactUpdated = await ContactModel.findOneAndUpdate({ "contact.identity": identity, instance }, { contact, updatedAt: new Date() }, { new: true })

        if (contactUpdated)
            newContact = contactUpdated
        else {
            let { contact: contactCreated } = await createContact({ instance, contact })
            newContact = contactCreated
        }

        return { contact: newContact }
    } catch (error) {
        throw error
    }
}

export const updateContact = async (_id: mongoose.Types.ObjectId, data: any) => {
    try {
        let contact = await ContactModel.findByIdAndUpdate(_id, data, { new: true })
        return { contact }
    } catch (error) {
        throw error
    }
}

export const updateContactsFromBlip = async (
    {
        instance: instanceId,
        lastUpdate,
        page = 1,
        take = defaultTakeContact,
        migration
    }: {
        instance: string | mongoose.Types.ObjectId,
        lastUpdate?: Date,
        page?: number
        take?: number,
        migration?: IMigration
    }
) => {
    let finished = undefined
    try {
        console.log("{ updateContactsFromBlip }", { instanceId, lastUpdate, page, take, migration })
        let now = migration ? migration.createdAt : new Date()

        if (migration) {
            if (migration.page) page = migration.page
            if (migration.take) take = migration.take
        } else {
            let { migration: m } = await createMigration({
                instance: instanceId,
                type: "contact",
                take: defaultTakeContact
            })

            migration = m
        }

        let { instance } = await getInstanceById(instanceId)

        if (!instance)
            throw { message: "Instância não encontrada" }

        let skip = (page - 1) * take

        let result: any = { quantity: 0 }

        if (migration && ["pending", "waiting"].includes(migration.status))
            await updateMigration(migration._id, { status: "processing", updatedAt: new Date() })

        let { items: contacts, total: totalSize } = await getContacts(
            { token: instance.token, tenantId: instance.client_tenant },
            {
                skip,
                take,
                filter: lastUpdate ? `(lastmessagedate%20ge%20datetimeoffset'${moment(lastUpdate).toISOString()}')` : undefined//`(lastmessagedate%20le%20datetimeoffset'${moment().toISOString()}')`
            }
        )

        console.log({ page, take, totalSize })

        if (migration && migration.totalSize != totalSize)
            await updateMigration(migration._id, { totalSize, updatedAt: new Date() })

        result.quantity = totalSize

        await Promise.all(contacts.map((contact: any) =>
            updateOrCreateContact({ instance: instanceId, contact })
        ))

        console.log(`${totalSize > (take * page) ? take * page : totalSize} / ${totalSize}`)

        if (totalSize > (take * page)) {
            skip = take * page
            page++
            finished = false

            result = { ...result, page, finished }
        } else {
            finished = true
            result = { ...result, finished }
        }

        if (migration) {
            await updateMigration(migration._id, { status: finished ? "finished" : "waiting", page, updatedAt: new Date() })
        }

        if (finished)
            await updateInstance(instance._id, { "lastUpdate.contact": now })

        // console.log(result)

        return result
    } catch (error) {
        console.error(error)
        // if (finished === undefined || finished === false)
        //     executeExternalContactUpdate({ instance: new mongoose.Types.ObjectId(instanceId) })

        throw error
    }
}

export const executeExternalContactUpdate = async ({ instance }: { instance: mongoose.Types.ObjectId }) => {
    try {
        return axios.post(`${API_URL}/contacts/update/${instance.toString()}`)
    } catch (error) {
        throw error
    }
}