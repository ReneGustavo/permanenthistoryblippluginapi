import mongoose, { Schema, model } from 'mongoose';
import { getWebhookReference } from '../helpers/blip/utils';
import ContactModel, { updateContact } from './contact';
import Chat, { IChat } from '../classes/chat';
import { getMessagesByIdentity } from '../helpers/blip/messages';
import { getInstanceById, updateInstance } from './instance';
import { IMigration } from '../classes/migration';
import { createMigration, updateMigration } from './migration';
import axios from 'axios';

const { API_URL } = process.env

// SCHEMA

export const defaultTakeChat = 100;

export const ChatSchema = new Schema({
    _id: {
        type: mongoose.Schema.Types.ObjectId,
        required: true,
    },
    createdAt: {
        type: Date,
        required: true,
    },
    updatedAt: {
        type: Date,
        required: false,
    },

    instance: {
        type: mongoose.Schema.Types.ObjectId,
        required: true,
        ref: "Instance"
    },

    body: {
        type: Object,
        required: false
    },
    query: {
        type: Object,
        required: false
    },
    reference: {
        type: Object,
        required: false/*{
            type: {
                type: String,
                required: false
            },
            origin: {
                type: String,
                required: false
            },
            identity: {
                type: String,
                required: false
            }
        }*/
    }
});

const ChatModel = model<IChat>('Chat', ChatSchema);
export default ChatModel;

export const createChat = async ({
    instance,
    body,
    query
}: {
    instance: string | mongoose.Types.ObjectId,
    body: any,
    query?: any
}) => {
    try {
        const reference = getWebhookReference(body)

        let chat = await ChatModel.create(new Chat({
            _id: new mongoose.Types.ObjectId(),
            createdAt: new Date(),

            instance: new mongoose.Types.ObjectId(instance),

            body,
            query,

            reference
        }))
    } catch (error) {
        throw error
    }
}

export const updateChatFromBlipByIdentity = async ({ identity, lastSync, instance, token, tenantId }: { identity: string, lastSync?: Date, instance: mongoose.Types.ObjectId, token?: string, tenantId?: string }) => {
    try {
        if (!token || !tenantId) {
            let { instance: inst } = await getInstanceById(instance)
            if (inst) {
                token = inst.token
                tenantId = inst.client_tenant
            }
        }

        token = token as string
        tenantId = tenantId as string

        let { items } = await getMessagesByIdentity({ token, tenantId }, { identity, storageDate: lastSync })

        let chat = await ChatModel.find({
            instance: new mongoose.Types.ObjectId(instance),
            "reference.identity": identity
        })
        let ids = chat.map(({ reference }) => reference?.id).filter((item) => item)

        await ChatModel.insertMany(
            items
                .filter((item: any) => {
                    let reference = { ...getWebhookReference(item), identity }
                    return !reference.id || !ids.includes(reference.id)
                })
                .map((item: any) => {
                    let reference = { ...getWebhookReference(item), identity }

                    return new Chat({
                        _id: new mongoose.Types.ObjectId(),
                        createdAt: new Date(),

                        instance: new mongoose.Types.ObjectId(instance),

                        body: item,

                        reference
                    })
                }
                )
        )
    } catch (error) {
        throw error
    }
}

export const updateChatFromBlip = async ({
    instance: instanceId,
    page = 1,
    limit = defaultTakeChat,
    where: whereAttr,
    migration
}: {
    instance: string | mongoose.Types.ObjectId,
    page?: number,
    limit?: number,
    where?: { [key: string]: any },
    migration?: IMigration
}) => {
    try {
        if (!migration) {
            let { migration: m } = await createMigration({
                instance: instanceId,
                type: "chat",
                take: defaultTakeChat
            })

            migration = m

            if (migration.page) page = migration.page
            if (migration.take) limit = migration.take
        }

        let { instance }: { instance: any } = await getInstanceById(instanceId)

        if (!instance)
            throw { message: "Instância não encontrada" }

        let now = migration ? migration.createdAt : new Date()

        let interval = new Date((new Date()).getTime() - (24 * 60 * 60 * 1000)) // 24 horas
        // let interval = new Date((new Date()).getTime() - (2 * 60 * 1000)) // 5 minutos

        let where = {
            instance: instance._id,
            $or: [
                { lastSync: undefined },
                { lastSync: { $lte: interval } }
            ],
            ...whereAttr
        }

        let totalSize = await ContactModel.countDocuments(where)

        if (migration && ["pending", "waiting"].includes(migration.status))
            await updateMigration(migration._id, { totalSize, status: "processing", updatedAt: new Date() })

        let qtd = 0;

        let finished = undefined

        for (let i = page; i <= (migration ? page : Math.ceil(totalSize / limit)); i++) {
            let contacts = await ContactModel
                .find(where)
                .skip(limit * (page - 1))
                .limit(limit)
                .select("_id contact.identity lastSync")

            await Promise.all(
                contacts.map(async (contact) => {
                    try {
                        console.log(`${++qtd} / ${totalSize}`)
                        await updateChatFromBlipByIdentity({
                            identity: contact.contact.identity,
                            token: instance.token,
                            lastSync: contact.lastSync,
                            instance: instance._id
                        })
                        await updateContact(contact._id, { lastSync: new Date() })
                    } catch { }
                })
            )

            // if (migration)
            //     await updateMigration(migration._id, { page: ++i, updatedAt: new Date() })
        }

        // finished = migration ? (page >= Math.ceil(totalSize / limit)) : true
        finished = migration ? totalSize === 0 : true

        await updateInstance(instance._id, { "lastUpdate.chat": now })

        if (migration)
            await updateMigration(migration._id, { status: finished ? "finished" : "waiting", updatedAt: new Date() })

        return { finished }
    } catch (error) {
        throw error
    }
}

export const executeExternalChatUpdate = async ({ instance }: { instance: mongoose.Types.ObjectId }) => {
    try {
        return axios.post(`${API_URL}/chat/update/${instance.toString()}`)
    } catch (error) {
        throw error
    }
}