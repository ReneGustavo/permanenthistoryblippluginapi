import mongoose, { Schema, model } from 'mongoose';
import Migration, { IMigration } from '../classes/migration';
import { defaultTakeContact, updateContactsFromBlip } from './contact';
import { MigrationTypes } from '../types/migration.types';
import { defaultTakeChat, updateChatFromBlip } from './chat';
import { updateComsumption } from './instance';

// SCHEMA

export const MigrationSchema = new Schema({
    _id: {
        type: mongoose.Schema.Types.ObjectId,
        required: true,
    },
    createdAt: {
        type: Date,
        required: true,
    },
    updatedAt: {
        type: Date,
        required: false,
    },

    status: {
        type: String,
        required: true,
        enum: ["pending", "processing", "waiting", "finished", "canceled"]
    },

    instance: {
        type: mongoose.Schema.Types.ObjectId,
        required: true,
        ref: "Instance"
    },

    type: {
        type: String,
        required: true,
        enum: ["contact", "chat"]
    },

    filter: {
        type: Object,
        required: false
    },

    page: {
        type: Number,
        required: true,
        default: 1
    },
    take: {
        type: Number,
        required: true
    },
    totalSize: {
        type: Number,
        required: false
    },

    nextMigration: {
        type: String,
        required: false,
        enum: ["contact", "chat"]
    }
});

const MigrationModel = model<IMigration>('Migration', MigrationSchema);
export default MigrationModel;

export const createMigration = async ({
    instance,
    type,
    filter,
    take,
    nextMigration,
    run = false
}: {
    instance: string | mongoose.Types.ObjectId,
    type: MigrationTypes,
    filter?: { [key: string]: any },
    take: number,
    nextMigration?: MigrationTypes,
    run?: boolean
}) => {
    try {
        let migration = await MigrationModel.create(new Migration({
            _id: new mongoose.Types.ObjectId(),
            createdAt: new Date(),

            instance: new mongoose.Types.ObjectId(instance),

            status: "pending",

            type,
            filter,
            take,

            nextMigration
        }))

        if (run)
            setTimeout(() => runMigration({ migration }), 0)

        return { migration }
    } catch (error) {
        throw error
    }
}

export const updateMigration = async (_id: mongoose.Types.ObjectId, data: { [key: string]: any }) => {
    try {
        let migration = await MigrationModel.findByIdAndUpdate(_id, data, { new: true })

        if (!migration)
            throw { message: "Migração não encontrada" }

        if (data?.status == "finished") {
            await updateComsumption(migration.instance)

            if (migration.nextMigration) {
                await createMigration({
                    instance: migration.instance,
                    type: migration.nextMigration,
                    filter: {},
                    take: migration.nextMigration == "chat" ? defaultTakeChat : defaultTakeContact,
                    run: true
                })
            }
        }

        return { migration }
    } catch (error) {
        throw error
    }
}

export const getMigrationToRunByInstance = async ({
    instance,
    type
}: {
    instance: string | mongoose.Types.ObjectId,
    type?: MigrationTypes
}) => {
    try {
        let where: { [key: string]: any } = {
            instance: new mongoose.Types.ObjectId(instance),
            status: { $in: ["pending", "waiting", "processing"] }
        }

        if (type) where.type = type

        let migration = await MigrationModel
            .findOne(where)
            .sort("createdAt") || undefined

        return { migration }
    } catch (error) {
        throw error
    }
}

export const runMigration = async ({
    migration
}: {
    migration: IMigration
}) => {
    try {
        let { instance, type, filter, page, take } = migration

        if (type == "contact") {
            await updateContactsFromBlip({ instance, page, take, migration, ...filter })
        } else if (type == "chat") {
            await updateChatFromBlip({ instance, page, limit: take, migration })
        }
    } catch (error) {
        throw error
    }
}