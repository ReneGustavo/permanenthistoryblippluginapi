import mongoose, { Schema, model } from 'mongoose';
import Instance, { IInstance } from '../classes/instance';
import ContactModel from './contact';
import ChatModel from './chat';
import { getPlanById } from './plan';

export const InstanceSchema = new Schema({
  _id: {
    type: mongoose.Schema.Types.ObjectId,
    required: true,
  },
  created_at: {
    type: Date,
    required: true,
  },
  updated_at: {
    type: Date,
    required: false,
  },
  deleted_at: {
    type: Date,
    required: false,
  },
  status: {
    type: Number,
    required: true,
    default: 1,
  },

  execution_token: {
    type: String,
    required: true,
  },
  token: {
    type: String,
    required: true,
  },

  client_tenant: {
    type: String,
    required: true
  },
  client_access_key: {
    type: String,
    required: true
  },
  client_short_name: {
    type: String,
    required: true
  },

  plan: {
    type: mongoose.Schema.Types.ObjectId,
    required: true,
    ref: "Plan"
  },
  contacts_consumption: {
    type: Number,
    required: true
  },
  messages_consumption: {
    type: Number,
    required: true
  },

  lastUpdate: {
    type: Object,
    required: false
  }
});
const InstanceModel = model<IInstance>('Instance', InstanceSchema);
export default InstanceModel;

export const getInstances = async (where: { [key: string]: any } = {}): Promise<{ instances: IInstance[] }> => {
  try {
    let instances = await InstanceModel
      .find({ ...where, deletedAt: undefined, deleted_at: undefined })
      .populate({ path: "plan", select: "_id name contacts_limit messages_limit" })
      .lean()
    return { instances }
  } catch (error) {
    throw error
  }
}

export const getInstanceById = async (_id: mongoose.Types.ObjectId | string) => {
  try {
    let instance = await InstanceModel
      .findOne({ _id: new mongoose.Types.ObjectId(_id), deletedAt: undefined, deleted_at: undefined })
      .populate({ path: "plan", select: "_id name contacts_limit messages_limit" })
      .lean()
    return { instance }
  } catch (error) {
    throw error
  }
}

export const getInstanceByToken = async (token: string) => {
  try {
    let instance = await InstanceModel
      .findOne({ token })
      .populate({ path: "plan", select: "_id name contacts_limit messages_limit" })
      .lean()

    return { instance }
  } catch (error) {
    throw error
  }
}

export const createInstance = async ({
  execution_token,
  token,
  client_tenant,
  client_access_key,
  client_short_name,
  plan
}: {
  execution_token: string,
  token: string,
  client_tenant: string,
  client_access_key: string,
  client_short_name: string,
  plan: mongoose.Types.ObjectId
}) => {
  try {
    let _id = new mongoose.Types.ObjectId()

    if (!(await getPlanById(plan))?.plan)
      throw { message: "Plano não existente" }

    let instance = await InstanceModel.create(new Instance({
      _id,
      created_at: new Date(),
      status: 1,

      execution_token,
      token,

      client_tenant,
      client_access_key,
      client_short_name,

      plan,
      contacts_consumption: 0,
      messages_consumption: 0
    }))

    return instance
  } catch (error) {
    throw error
  }
}

export const updateInstance = async (
  _id: mongoose.Types.ObjectId | string,
  data: { [key: string]: any }
) => {
  try {
    let instance = await InstanceModel.findByIdAndUpdate(_id, {
      updated_at: new Date(),
      ...data
    }, { new: true })

    return instance
  } catch (error) {
    throw error
  }
}

export const updateComsumption = async (instance: mongoose.Types.ObjectId) => {
  try {
    let inst = await InstanceModel.findById(instance)

    if (!inst)
      throw { message: "Instancâ não encontrada" }

    let where = {
      client_tenant: inst.client_tenant
    }

    let instances = await InstanceModel.find({ deletedAt: undefined, status: 1, ...where })

    let whereCount = {
      instance: {
        $in: instances.map(({ _id }) => _id)
      }
    }

    let [
      contacts_consumption,
      messages_consumption
    ] = await Promise.all([
      ContactModel.countDocuments(whereCount),
      ChatModel.countDocuments(whereCount)
    ])

    let data = { contacts_consumption, messages_consumption }

    await InstanceModel.findByIdAndUpdate(instance, data)

    return data
  } catch (error) {
    throw error
  }
}