import mongoose, { Schema, model } from 'mongoose';
import { IPlan } from '../classes/plan';

export const PlanSchema = new Schema({
  _id: {
    type: mongoose.Schema.Types.ObjectId,
    required: true,
  },
  createdAt: {
    type: Date,
    required: true,
  },
  updatedAt: {
    type: Date,
    required: false,
  },
  deletedAt: {
    type: Date,
    required: false,
  },
  status: {
    type: Number,
    required: true,
    default: 1,
  },

  name: {
    type: String,
    required: true
  },
  contacts_limit: {
    type: Number,
    required: true
  },
  messages_limit: {
    type: Number,
    required: true
  }
});
const PlanModel = model<IPlan>('Plan', PlanSchema);
export default PlanModel;

export const getPlanById = async (_id: mongoose.Types.ObjectId | string) => {
  try {
    let plan = await PlanModel
      .findOne({ _id: new mongoose.Types.ObjectId(_id), deletedAt: undefined })
      .lean()
    return { plan }
  } catch (error) {
    throw error
  }
}