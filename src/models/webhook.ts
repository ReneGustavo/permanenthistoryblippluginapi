import mongoose, { Schema, model } from 'mongoose';
import Webhook, { IWebhook } from '../classes/webhook';
import { getWebhookReference } from '../helpers/blip/utils';
import { updateOrCreateContact } from './contact';
import { createChat } from './chat';

const { WEBHOOK_SAVE } = process.env

// SCHEMA

export const WebhookSchema = new Schema({
    _id: {
        type: mongoose.Schema.Types.ObjectId,
        required: true,
    },
    createdAt: {
        type: Date,
        required: true,
    },
    updatedAt: {
        type: Date,
        required: false,
    },

    instance: {
        type: mongoose.Schema.Types.ObjectId,
        required: true,
        ref: "Instance"
    },

    body: {
        type: Object,
        required: false
    },
    query: {
        type: Object,
        required: false
    },
    reference: {
        type: Object,
        required: false/*{
            type: {
                type: String,
                required: false
            },
            origin: {
                type: String,
                required: false
            },
            identity: {
                type: String,
                required: false
            }
        }*/
    }
});

const WebhookModel = model<IWebhook>('Webhook', WebhookSchema);
export default WebhookModel;

export const createWebhook = async ({
    instance,
    body,
    query
}: {
    instance: string | mongoose.Types.ObjectId,
    body: any,
    query?: any
}) => {
    try {
        const reference = getWebhookReference(body)

        let webhook = new Webhook({
            _id: new mongoose.Types.ObjectId(),
            createdAt: new Date(),

            instance: new mongoose.Types.ObjectId(instance),

            body,
            query,

            reference
        })

        if (WEBHOOK_SAVE === "true") {
            let webhookCreated = await WebhookModel.create(webhook)
            webhook = webhookCreated
        }

        onWebhook({ instance, webhook })
    } catch (error) {
        throw error
    }
}

export const onWebhook = async ({ instance, webhook }: { instance: string | mongoose.Types.ObjectId, webhook: IWebhook }) => {
    try {
        let { reference } = webhook

        if (reference?.type) {
            ({
                "contact": onWebhook_contact,
                // "message": onWebhook_message,
                // "attendanceRedirect": onWebhook_attendanceRedirect
            } as any)?.[reference.type]?.({ instance, webhook })
        }
    } catch (error) {
        throw error
    }
}

export const onWebhook_contact = async ({ instance, webhook }: { instance: string | mongoose.Types.ObjectId, webhook: IWebhook }) => {
    try {
        let { body } = webhook

        await updateOrCreateContact({ instance, contact: body })
    } catch (error) {
        throw error
    }
}

export const onWebhook_message = async ({ instance, webhook }: { instance: string | mongoose.Types.ObjectId, webhook: IWebhook }) => {
    try {
        let { body } = webhook

        await createChat({ instance, body })
    } catch (error) {
        throw error
    }
}

export const onWebhook_attendanceRedirect = async ({ instance, webhook }: { instance: string | mongoose.Types.ObjectId, webhook: IWebhook }) => {
    try {
        let { body } = webhook

        await createChat({ instance, body })
    } catch (error) {
        throw error
    }
}