'use strict';
import { Router } from 'express';
import controller from '../controllers/chatController';
import middlewares from '../helpers/middlewares';

const router = Router();

router.get('/:identity',
    [middlewares.middlewareInstance],
    controller.get
);

router.post('/update/:instance',
    [],
    controller.update
);

export default router;
