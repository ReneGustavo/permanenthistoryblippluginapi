'use strict';
import { Router } from 'express';
import controller from '../controllers/instanceController';
import middlewares from '../helpers/middlewares';

const router = Router();

// TODO: VALIDATOR
// import validator from '../../validators/pessoa';

router.get('/',
    // [middlewares.decode],
    controller.get
);

router.post('/',
    // [middlewares.decode],
    controller.post
);

router.put('/',
    // [middlewares.decode],
    controller.put
);

router.put('/update/webhook',
    [middlewares.middlewareInstance],
    controller.updateWebhook
);

export default router;