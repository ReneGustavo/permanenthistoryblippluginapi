'use strict';
import { Request, Response, Router } from 'express';
import mongoose from 'mongoose';

const router = Router();

import webhookRouter from './webhook';
router.use('/webhook', webhookRouter);

import contactsRouter from './contacts';
router.use('/contacts', contactsRouter);

import chatRouter from './chat';
router.use('/chat', chatRouter);

import instancesRouter from './instances';
router.use('/instances', instancesRouter);

import cronRouter from './cron';
router.use('/cron', cronRouter);

router.get("/health", (req: Request | any, res: Response) => res.status(200).json({ success: true, health: { api: true, database: mongoose.connection.readyState === 1 } }))

export default router;

