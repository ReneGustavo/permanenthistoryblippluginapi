'use strict';
import { Router } from 'express';
import controller from '../controllers/contactController';
import middlewares from '../helpers/middlewares';

const router = Router();

router.get('/',
    [middlewares.middlewareInstance],
    controller.list
);

router.post('/update/:instance',
    [],
    controller.update
);

export default router;
