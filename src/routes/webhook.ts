'use strict';
import { Router } from 'express';
import controller from '../controllers/webhookController';

const router = Router();

router.post('/:instance_id',
    [],
    controller.post
);

export default router;
