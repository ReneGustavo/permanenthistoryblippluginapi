'use strict';
import { Router } from 'express';
import controller from '../controllers/cronController';
import middlewares from '../helpers/middlewares';

const router = Router();

router.get('/routine/update-chats', [], controller.routineUpdatedChats);
router.post('/routine/update-chats', [], controller.routineUpdatedChats);

router.get('/routine/update-contacts', [], controller.routineUpdatedContacts);
router.post('/routine/update-contacts', [], controller.routineUpdatedContacts);

export default router;
