process.env.TZ = 'America/Sao_Paulo';

export const maxDuration = 300;

'use strict';
import 'dotenv/config';

import express from 'express';
import cors from 'cors';
import bodyParser from 'body-parser';
import routes from './routes';
import middlewares from './helpers/middlewares';
import db from './helpers/database';

import * as moment from 'moment-timezone';
moment.tz.setDefault("America/Sao_Paulo");

db.connect()

// EXPRESS
const app: express.Express = express();

// CORS
const options: cors.CorsOptions = {
    allowedHeaders: ['X-Requested-With', 'Content-Type'],
    credentials: true,
    methods: 'GET,HEAD,OPTIONS,PUT,PATCH,POST,DELETE',
    origin: '*',
};
// app.use(cors(options));
app.use(cors());
// app.use(formidable());

// Parse de json e urlencoded form
app.use(bodyParser.json({ limit: '50mb' }))
app.use(bodyParser.urlencoded({ limit: '50mb', extended: true }))

// // Middlewares
// app.use(middlewares.basicAuth)

// Rotas
app.use('/api', routes);

export default app;