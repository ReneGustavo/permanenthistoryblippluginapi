'use strict';
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const chatController_1 = __importDefault(require("../controllers/chatController"));
const middlewares_1 = __importDefault(require("../helpers/middlewares"));
const router = (0, express_1.Router)();
router.get('/:identity', [middlewares_1.default.middlewareInstance], chatController_1.default.get);
router.post('/update/:instance', [], chatController_1.default.update);
exports.default = router;
