'use strict';
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const cronController_1 = __importDefault(require("../controllers/cronController"));
const router = (0, express_1.Router)();
router.get('/routine/update-chats', [], cronController_1.default.routineUpdatedChats);
router.post('/routine/update-chats', [], cronController_1.default.routineUpdatedChats);
router.get('/routine/update-contacts', [], cronController_1.default.routineUpdatedContacts);
router.post('/routine/update-contacts', [], cronController_1.default.routineUpdatedContacts);
exports.default = router;
