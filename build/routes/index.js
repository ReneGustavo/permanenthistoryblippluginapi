'use strict';
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const router = (0, express_1.Router)();
const webhook_1 = __importDefault(require("./webhook"));
router.use('/webhook', webhook_1.default);
const contacts_1 = __importDefault(require("./contacts"));
router.use('/contacts', contacts_1.default);
const chat_1 = __importDefault(require("./chat"));
router.use('/chat', chat_1.default);
const instances_1 = __importDefault(require("./instances"));
router.use('/instances', instances_1.default);
const cron_1 = __importDefault(require("./cron"));
router.use('/cron', cron_1.default);
exports.default = router;
