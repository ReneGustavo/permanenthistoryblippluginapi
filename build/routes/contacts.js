'use strict';
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const contactController_1 = __importDefault(require("../controllers/contactController"));
const middlewares_1 = __importDefault(require("../helpers/middlewares"));
const router = (0, express_1.Router)();
router.get('/', [middlewares_1.default.middlewareInstance], contactController_1.default.list);
router.post('/update/:instance', [], contactController_1.default.update);
exports.default = router;
