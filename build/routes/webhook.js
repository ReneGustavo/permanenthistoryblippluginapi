'use strict';
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const webhookController_1 = __importDefault(require("../controllers/webhookController"));
const router = (0, express_1.Router)();
router.post('/:instance_id', [], webhookController_1.default.post);
exports.default = router;
