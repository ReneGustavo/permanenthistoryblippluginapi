'use strict';
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const instanceController_1 = __importDefault(require("../controllers/instanceController"));
const middlewares_1 = __importDefault(require("../helpers/middlewares"));
const router = (0, express_1.Router)();
// TODO: VALIDATOR
// import validator from '../../validators/pessoa';
router.get('/', 
// [middlewares.decode],
instanceController_1.default.get);
router.post('/', 
// [middlewares.decode],
instanceController_1.default.post);
router.put('/', 
// [middlewares.decode],
instanceController_1.default.put);
router.put('/update/webhook', [middlewares_1.default.middlewareInstance], instanceController_1.default.updateWebhook);
exports.default = router;
