"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.getContacts = void 0;
const _1 = require(".");
const contact_1 = require("../../models/contact");
const getContacts = ({ token, tenantId }, { skip = 0, take = contact_1.defaultTakeContact, // Entre 1 e 30000
filter }) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        let queries = [
            ["$skip", skip],
            ["$take", take],
            ["$filter", filter]
        ].filter(([key, value]) => value != undefined);
        console.log(queries);
        let { data } = yield (0, _1.runCommand)({
            token,
            tenantId,
            to: "postmaster@crm.msging.net",
            method: "get",
            uri: `/contacts?${queries.map(([key, value]) => `${key}=${value}`).join("&")}`
        });
        console.log("dataaa", data);
        let { resource } = data;
        return resource;
    }
    catch (error) {
        throw error;
    }
});
exports.getContacts = getContacts;
