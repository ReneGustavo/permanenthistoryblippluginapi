"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __rest = (this && this.__rest) || function (s, e) {
    var t = {};
    for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p) && e.indexOf(p) < 0)
        t[p] = s[p];
    if (s != null && typeof Object.getOwnPropertySymbols === "function")
        for (var i = 0, p = Object.getOwnPropertySymbols(s); i < p.length; i++) {
            if (e.indexOf(p[i]) < 0 && Object.prototype.propertyIsEnumerable.call(s, p[i]))
                t[p[i]] = s[p[i]];
        }
    return t;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.setWebhook = exports.getAdvancedConfiguration = exports.setAdvancedConfiguration = exports.setResource = exports.checkConnectivity = exports.runCommand = exports.authorizationEncode = exports.authorizationDecode = void 0;
const util_1 = require("../util");
const api_1 = __importDefault(require("./api"));
const axios_1 = __importDefault(require("axios"));
const { PROXY = "" } = process.env;
const authorizationDecode = (token) => {
    var _a;
    token = token.replace("Key ", "");
    let [shortName, accessKeyEncoded] = (_a = (0, util_1.atob)(token)) === null || _a === void 0 ? void 0 : _a.split(":");
    let accessKey = (0, util_1.btoa)(accessKeyEncoded);
    return { shortName, accessKey };
};
exports.authorizationDecode = authorizationDecode;
const authorizationEncode = (shortName, accessKey) => {
    return `Key ${(0, util_1.btoa)(`${shortName}:${(0, util_1.atob)(accessKey)}`)}`;
};
exports.authorizationEncode = authorizationEncode;
const runCommand = (_a) => __awaiter(void 0, void 0, void 0, function* () {
    var { token, tenantId, action = "blip_api", path = "commands", id = "{{$guid}}", to, method, uri, http } = _a, rest = __rest(_a, ["token", "tenantId", "action", "path", "id", "to", "method", "uri", "http"]);
    try {
        let result;
        console.log("runCommand", action, path, http);
        if (action == "blip_api") {
            result = yield api_1.default.post(`${PROXY}https://${tenantId}.http.msging.net/${path}`, Object.assign({ id,
                to,
                method,
                uri }, rest), {
                headers: {
                    "Content-Type": "application/json",
                    Authorization: token
                }
            });
        }
        else if (action == "http") {
            console.log("HTTP REQ", http);
            result = yield (0, axios_1.default)(http);
        }
        else
            throw { message: "Action não reconhecido" };
        return result;
    }
    catch (error) {
        // console.error(error)
        throw error;
    }
});
exports.runCommand = runCommand;
const checkConnectivity = ({ token, tenantId }) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        let { data } = yield (0, exports.runCommand)({
            token,
            tenantId,
            to: "postmaster@msging.net",
            method: "get",
            uri: "/ping"
        });
        return data;
    }
    catch (error) {
        throw error;
    }
});
exports.checkConnectivity = checkConnectivity;
const setResource = ({ token, tenantId }, { type, key, resource }) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        let { data } = yield (0, exports.runCommand)({
            token,
            tenantId,
            to: "postmaster@msging.net",
            method: "set",
            uri: `/resources/${key}`,
            type,
            resource
        });
        return data;
    }
    catch (error) {
        throw error;
    }
});
exports.setResource = setResource;
const setAdvancedConfiguration = ({ token, tenantId }, resource) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        let { data } = yield (0, exports.runCommand)({
            token,
            tenantId,
            method: "set",
            uri: "lime://postmaster@analytics.msging.net/configuration",
            type: "application/json",
            resource
        });
        return data;
    }
    catch (error) {
        throw error;
    }
});
exports.setAdvancedConfiguration = setAdvancedConfiguration;
const getAdvancedConfiguration = ({ token, tenantId }) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        let { data } = yield (0, exports.runCommand)({
            token,
            tenantId,
            method: "get",
            uri: "lime://postmaster@analytics.msging.net/configuration",
            resource: {}
        });
        console.log(data);
        if ((data === null || data === void 0 ? void 0 : data.status) != "success")
            throw { status: 400 };
        let { resource } = data;
        return resource;
    }
    catch (error) {
        throw error;
    }
});
exports.getAdvancedConfiguration = getAdvancedConfiguration;
const setWebhook = ({ token, tenantId }, isValid, url, replace = false) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        let urls = Array.isArray(url) ? url : [url];
        if (!replace) {
            let resource = {};
            try {
                resource = yield (0, exports.getAdvancedConfiguration)({ token, tenantId });
            }
            catch (_b) { }
            if (resource["Webhook.Url"]) {
                let urls2 = resource["Webhook.Url"].split(";").map((url) => url.trim());
                for (let url of urls) {
                    if (!urls2.includes(url))
                        urls2.push(url);
                }
                urls = urls2;
            }
        }
        return yield (0, exports.setAdvancedConfiguration)({ token, tenantId }, {
            "Webhook.IsValid": isValid ? "True" : "False",
            "Webhook.Url": urls.map(url => url.trim()).join(";")
        });
    }
    catch (error) {
        throw error;
    }
});
exports.setWebhook = setWebhook;
