"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.getWebhookReference = exports.getWebhookType = void 0;
const getWebhookType = (obj) => {
    try {
        if (typeof obj != "object")
            return undefined;
        if (obj === null || obj === void 0 ? void 0 : obj.content) {
            let { content } = obj;
            if (typeof content == "string")
                return "message";
            if (content.team)
                return "attendanceRedirect";
            return "message";
        }
        if ((obj === null || obj === void 0 ? void 0 : obj.lastMessageDate) && (obj === null || obj === void 0 ? void 0 : obj.identity)) {
            return "contact";
        }
    }
    catch (error) {
        throw error;
    }
};
exports.getWebhookType = getWebhookType;
const getWebhookReference = (obj) => {
    var _a, _b;
    try {
        let type = (0, exports.getWebhookType)(obj);
        let { from, id, to, direction, date, metadata } = obj;
        let datetimeReference = date || +(metadata === null || metadata === void 0 ? void 0 : metadata.date_processed) || +(metadata === null || metadata === void 0 ? void 0 : metadata.date_created);
        let idReference = id || (metadata === null || metadata === void 0 ? void 0 : metadata.$internalId);
        let reference = {
            type,
            datetime: datetimeReference ? new Date(datetimeReference) : undefined,
            id: idReference
        };
        if (from && to) {
            let origin = from.indexOf("@msging.net") > 0 ? "blip" : "user";
            let identity = origin == "blip" ? (_a = to === null || to === void 0 ? void 0 : to.split("/")) === null || _a === void 0 ? void 0 : _a[0] : (_b = from === null || from === void 0 ? void 0 : from.split("/")) === null || _b === void 0 ? void 0 : _b[0];
            reference.origin = origin;
            reference.identity = identity;
        }
        else if (direction) {
            let origin = direction === "sent" ? "blip" : "user";
            reference.origin = origin;
        }
        return reference;
    }
    catch (error) {
        throw error;
    }
};
exports.getWebhookReference = getWebhookReference;
