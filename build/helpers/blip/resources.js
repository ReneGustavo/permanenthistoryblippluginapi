"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.setResource = exports.getResource = void 0;
const _1 = require(".");
const getResource = ({ token, tenantId }, { key }) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        let { data } = yield (0, _1.runCommand)({
            token,
            tenantId,
            to: "postmaster@msging.net",
            method: "get",
            uri: `/resources/${key}`
        });
        return data.resource;
    }
    catch (error) {
        throw error;
    }
});
exports.getResource = getResource;
const setResource = ({ token, tenantId }, { type, key, resource }) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        let { data } = yield (0, _1.runCommand)({
            token,
            tenantId,
            to: "postmaster@msging.net",
            method: "set",
            uri: `/resources/${key}`,
            type,
            resource
        });
        return data;
    }
    catch (error) {
        throw error;
    }
});
exports.setResource = setResource;
