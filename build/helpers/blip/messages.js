"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.getMessagesByIdentity = exports.sendActiveMessage = exports.verifyIdentity = void 0;
const _1 = require(".");
const verifyIdentity = ({ token, tenantId }, phoneNumber) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        let { data } = yield (0, _1.runCommand)({
            token,
            tenantId,
            to: "postmaster@wa.gw.msging.net",
            method: "get",
            uri: `lime://wa.gw.msging.net/accounts/${phoneNumber}`
        });
        let { status, resource, reason } = data;
        if (status == "failure")
            throw { code: reason === null || reason === void 0 ? void 0 : reason.code, message: reason === null || reason === void 0 ? void 0 : reason.description };
        return resource;
    }
    catch (error) {
        throw error;
    }
});
exports.verifyIdentity = verifyIdentity;
const sendActiveMessage = ({ token, tenantId }, { phoneNumber, identity, type = "template", namespace, template_name, language = { code: "pt_BR", policy: "deterministic" }, components }) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        if (phoneNumber && !identity) {
            let resource = yield (0, exports.verifyIdentity)({ token, tenantId }, phoneNumber);
            if (!resource)
                throw { status: 400, message: `Destinatário desconhecido` };
            identity = resource.identity;
        }
        let result = yield (0, _1.runCommand)({
            token,
            tenantId,
            path: "messages",
            to: identity,
            type: "application/json",
            method: "post",
            content: {
                type,
                template: {
                    namespace,
                    name: template_name,
                    language,
                    components
                }
            }
        });
        let { data } = result;
        if (data == "")
            data = null;
        return data;
    }
    catch (error) {
        throw error;
    }
});
exports.sendActiveMessage = sendActiveMessage;
const getMessagesByIdentity = ({ token, tenantId }, { identity, take = 100, // Entre 1 e 100
refreshExpiredMedia = true, storageDate }) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        let queries = [
            ["$take", take],
            ["refreshExpiredMedia", refreshExpiredMedia ? "true" : "false"],
            ["storageDate", storageDate ? storageDate.toISOString() : undefined]
        ].filter(([key, value]) => value != undefined);
        identity = identity === null || identity === void 0 ? void 0 : identity.trim();
        let { data } = yield (0, _1.runCommand)({
            token,
            tenantId,
            method: "get",
            uri: `/threads/${identity}?${queries.map(([key, value]) => `${key}=${value}`).join("&")}`
        });
        // console.log(data)
        let { resource } = data;
        return resource;
    }
    catch (error) {
        throw error;
    }
});
exports.getMessagesByIdentity = getMessagesByIdentity;
