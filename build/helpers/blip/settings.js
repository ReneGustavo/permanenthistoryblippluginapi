"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.setWebhook = exports.getWebhook = void 0;
const _1 = require(".");
const getWebhook = ({ token, tenantId }) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        let { data } = yield (0, _1.runCommand)({
            token,
            tenantId,
            uri: "lime://postmaster@analytics.msging.net/configuration",
            method: "get",
            type: "application/json",
            resource: {}
        });
        return data;
    }
    catch (error) {
        throw error;
    }
});
exports.getWebhook = getWebhook;
const setWebhook = ({ token, tenantId }, { isValid, url, replaceUrl = false }) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        let obj = {};
        let webhook = yield (0, exports.getWebhook)({ token, tenantId });
        if (isValid != undefined)
            obj["Webhook.isValid"] = isValid ? "True" : "False";
        if (url)
            obj["Webhook.Url"] = replaceUrl ? url : [...(webhook["Webhook.Url"] || "").split(";"), url].join(";");
        let { data } = yield (0, _1.runCommand)({
            token,
            tenantId,
            uri: "lime://postmaster@analytics.msging.net/configuration",
            method: "set",
            type: "application/json",
            resource: obj
        });
        let { resource } = data;
        return resource;
    }
    catch (error) {
        throw error;
    }
});
exports.setWebhook = setWebhook;
