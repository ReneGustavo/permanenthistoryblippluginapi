"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.atob = exports.btoa = exports.generatePossiblePhones = exports.isCurrentDateBetween = exports.PromiseAllInChunks = exports.isEqual = exports.joinDirectory = exports.removeFalseFromObject = exports.addPeriod = exports.mergeObjects = exports.addMinutesToDate = exports.hasPermission = exports.getValueByPath = exports.calculateTimeDifferenceInDays = exports.calculateTimeDifferenceInMinutes = exports.calculateTimeDifferenceInSeconds = void 0;
const path_1 = require("path");
const mongoose_1 = __importDefault(require("mongoose"));
const calculateTimeDifferenceInSeconds = (date1, date2) => {
    const timeDifferenceInMilliseconds = Math.abs(date2.getTime() - date1.getTime());
    const timeDifferenceInMinutes = timeDifferenceInMilliseconds / 1000;
    return timeDifferenceInMinutes;
};
exports.calculateTimeDifferenceInSeconds = calculateTimeDifferenceInSeconds;
const calculateTimeDifferenceInMinutes = (date1, date2) => {
    const timeDifferenceInMilliseconds = Math.abs(date2.getTime() - date1.getTime());
    const timeDifferenceInMinutes = timeDifferenceInMilliseconds / (60 * 1000);
    return timeDifferenceInMinutes;
};
exports.calculateTimeDifferenceInMinutes = calculateTimeDifferenceInMinutes;
const calculateTimeDifferenceInDays = (date1, date2, absolute = true) => {
    let result = Math.floor((date1.getTime() - date2.getTime()) / (24 * 60 * 60 * 1000));
    return absolute ? Math.abs(result) : result;
};
exports.calculateTimeDifferenceInDays = calculateTimeDifferenceInDays;
const getValueByPath = (object, path) => {
    if (!object)
        return null;
    const keys = path.split('.');
    let value = object;
    for (const key of keys) {
        if (value && typeof value === 'object' && key in value) {
            value = value[key];
        }
        else {
            return null;
        }
    }
    return value;
};
exports.getValueByPath = getValueByPath;
const hasPermission = (permissions, permission_keys, and = true) => {
    if (!permissions)
        return false;
    for (let permission_key of (Array.isArray(permission_keys) ? permission_keys : [permission_keys])) {
        if (!(0, exports.getValueByPath)(permissions, permission_key))
            return false;
    }
    return true;
};
exports.hasPermission = hasPermission;
const addMinutesToDate = (date, minutes) => {
    const novaData = new Date(date.getTime() + minutes * 60000); // 60000 milissegundos em 1 minuto
    return novaData;
};
exports.addMinutesToDate = addMinutesToDate;
const mergeObjects = (obj1, obj2) => {
    if (typeof obj1 !== 'object' || typeof obj2 !== 'object') {
        return obj2;
    }
    const result = Object.assign({}, obj1);
    for (const key in obj2) {
        if (obj2.hasOwnProperty(key)) {
            result[key] = (0, exports.mergeObjects)(obj1[key], obj2[key]);
        }
    }
    return result;
};
exports.mergeObjects = mergeObjects;
const addPeriod = (date, amount, unit) => {
    if (date instanceof Date) {
        const newDate = new Date(date);
        switch (unit) {
            case 'year':
                newDate.setFullYear(newDate.getFullYear() + amount);
                break;
            case 'month':
                newDate.setMonth(newDate.getMonth() + amount);
                break;
            case 'week':
                newDate.setDate(newDate.getDate() + amount * 7);
                break;
            case 'day':
                newDate.setDate(newDate.getDate() + amount);
                break;
            case 'hour':
                newDate.setHours(newDate.getHours() + amount);
                break;
            case 'minute':
                newDate.setMinutes(newDate.getMinutes() + amount);
                break;
            case 'second':
                newDate.setSeconds(newDate.getSeconds() + amount);
                break;
            case 'millisecond':
                newDate.setMilliseconds(newDate.getMilliseconds() + amount);
                break;
            default:
                throw new Error('Unrecognized unit: ' + unit);
        }
        return newDate;
    }
    else if (typeof date === 'object' && typeof date.add === 'function') {
        return date.clone().add(amount, unit);
    }
    else {
        throw new Error('Unsupported date type');
    }
};
exports.addPeriod = addPeriod;
const removeFalseFromObject = (obj) => {
    if (Array.isArray(obj)) {
        // Se for um array, filtre os elementos que não sejam "false"
        const newArray = obj.filter((item) => item !== false);
        // Chame a função recursivamente para cada elemento do novo array
        return newArray.map((item) => (0, exports.removeFalseFromObject)(item));
    }
    else if (typeof obj === "object" && obj !== null) {
        // Se for um objeto, filtre as propriedades cujo valor não seja "false"
        const newObj = {};
        for (const key in obj) {
            if (obj.hasOwnProperty(key) && obj[key] !== false) {
                newObj[key] = (0, exports.removeFalseFromObject)(obj[key]);
            }
        }
        return newObj;
    }
    else {
        // Caso contrário, retorne o valor sem modificação
        return obj;
    }
};
exports.removeFalseFromObject = removeFalseFromObject;
const joinDirectory = (...args) => {
    let args2 = [];
    for (let arg of args)
        args2 = [...args2, ...arg.split("/")];
    return (0, path_1.join)(process.cwd(), ...args2);
};
exports.joinDirectory = joinDirectory;
const isEqual = (_ids) => {
    for (let [index, _id] of _ids.entries()) {
        if (index > 0 && !(new mongoose_1.default.Types.ObjectId(_id)).equals(new mongoose_1.default.Types.ObjectId(_ids === null || _ids === void 0 ? void 0 : _ids[0])))
            return false;
    }
    return true;
};
exports.isEqual = isEqual;
const PromiseAllInChunks = (promises, chunkSize) => __awaiter(void 0, void 0, void 0, function* () {
    const results = [];
    // Função auxiliar para processar um chunk de promises
    const processChunk = (chunk) => __awaiter(void 0, void 0, void 0, function* () {
        for (const promise of chunk) {
            try {
                const result = yield promise;
                results.push(result);
            }
            catch (error) {
                // Trate os erros conforme necessário
                console.error(`Error in promise: ${error}`);
            }
        }
    });
    // Divide as promises em chunks
    for (let i = 0; i < promises.length; i += chunkSize) {
        const chunk = promises.slice(i, i + chunkSize);
        yield processChunk(chunk);
    }
    return results;
});
exports.PromiseAllInChunks = PromiseAllInChunks;
const isCurrentDateBetween = (startDate, endDate, dateToCompare = new Date()) => {
    startDate = new Date(startDate);
    endDate = new Date(endDate);
    dateToCompare = new Date(dateToCompare);
    return dateToCompare >= startDate && dateToCompare <= endDate;
};
exports.isCurrentDateBetween = isCurrentDateBetween;
const generatePossiblePhones = (phone) => {
    phone = phone.replace(/\D/g, '');
    let versions = [
        { useDDI: false, useNinth: true },
        { useDDI: false, useNinth: false },
        { useDDI: true, useNinth: true },
        { useDDI: true, useNinth: false },
    ];
    let phones = versions.map(({ useDDI, useNinth }) => {
        let { ddi = "55", ddd, number } = {};
        if (phone.length == 10) { // Sem DDI e sem 9
            ddd = phone.substring(0, 2);
            number = phone.slice(2);
        }
        else if (phone.length == 11) { // Sem DDI e com 9
            ddd = phone.substring(0, 2);
            number = phone.slice(3);
        }
        else if (phone.length == 12) { // Com DDI e sem 9
            ddi = phone.substring(0, 2);
            ddd = phone.substring(2, 4);
            number = phone.slice(4);
        }
        else if (phone.length == 13) { // Com DDI e com 9
            ddi = phone.substring(0, 2);
            ddd = phone.substring(2, 4);
            number = phone.slice(5);
        }
        let newPhone = "";
        if (useDDI)
            newPhone += ddi;
        newPhone += ddd;
        if (useNinth)
            newPhone += "9";
        newPhone += number;
        return newPhone;
    });
    return phones;
};
exports.generatePossiblePhones = generatePossiblePhones;
const btoa = (content) => {
    return Buffer.from(content).toString('base64');
};
exports.btoa = btoa;
const atob = (b64Encoded) => {
    return Buffer.from(b64Encoded, 'base64').toString();
};
exports.atob = atob;
