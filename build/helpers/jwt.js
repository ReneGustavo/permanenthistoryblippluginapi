"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.verifyJWT = exports.generateJWT = void 0;
const jsonwebtoken_1 = __importDefault(require("jsonwebtoken"));
const generateJWT = (json, expirar, secret = process.env.SECRET || 'secret') => __awaiter(void 0, void 0, void 0, function* () {
    let options = {};
    expirar && (options['expiresIn'] = expirar);
    return jsonwebtoken_1.default.sign(json, secret, options);
});
exports.generateJWT = generateJWT;
const verifyJWT = (token, secret = process.env.SECRET || 'secret') => __awaiter(void 0, void 0, void 0, function* () {
    if (!secret)
        throw { status: 400, message: 'Secret not found' };
    if (!token)
        throw { status: 401, message: 'Token not found' };
    return jsonwebtoken_1.default.verify(token, secret, function (err, decoded) {
        if (err)
            throw { status: 401, message: 'Invalid token', logout: true };
        return decoded;
    });
});
exports.verifyJWT = verifyJWT;
