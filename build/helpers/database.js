"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose_1 = __importDefault(require("mongoose"));
require("dotenv/config");
exports.default = {
    connect() {
        // DB settings
        const user = process.env.DATABASE_USER;
        const password = process.env.DATABASE_PASSWORD;
        const databaseName = process.env.DATABASE_NAME;
        const host = process.env.DATABASE_HOST;
        const uri = `mongodb+srv://${user}:${password}@${host}/${databaseName}?retryWrites=true&w=majority&authSource=admin`;
        return mongoose_1.default.connect(uri, {
        // Configurações de conexão
        });
    },
    disconnect() {
        return mongoose_1.default.disconnect();
    }
};
