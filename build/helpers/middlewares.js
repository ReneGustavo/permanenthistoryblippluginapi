"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
/* eslint-disable no-prototype-builtins */
require("dotenv/config");
const instance_1 = require("../models/instance");
class MiddlewaresController {
    basicAuth(req, res, next) {
        var _a;
        return __awaiter(this, void 0, void 0, function* () {
            try {
                let token;
                for (let key of ["authorization", "Authorization"]) {
                    if ((_a = req === null || req === void 0 ? void 0 : req.headers) === null || _a === void 0 ? void 0 : _a[key]) {
                        token = req.headers[key];
                        break;
                    }
                }
                token = token.replace("Bearer ", "");
                if (!token)
                    throw { status: 401, message: 'Authorization Token não encontrado', logout: true };
                if (token != process.env.AUTH_TOKEN && token != process.env.CRON_SECRET)
                    throw { status: 401, message: 'Authorization Token sem permissão', logout: true };
                if (next)
                    next();
            }
            catch (error) {
                return res.status((error === null || error === void 0 ? void 0 : error.status) || 500).json(error);
            }
        });
    }
    middlewareInstance(req, res, next) {
        var _a;
        return __awaiter(this, void 0, void 0, function* () {
            try {
                let token;
                for (let key of ["authorization", "Authorization"]) {
                    if ((_a = req === null || req === void 0 ? void 0 : req.headers) === null || _a === void 0 ? void 0 : _a[key]) {
                        token = req.headers[key];
                        break;
                    }
                }
                if (!token)
                    throw { status: 401, message: 'Authorization Token não encontrado', logout: true };
                let { instance } = yield (0, instance_1.getInstanceByToken)(token);
                if (!instance)
                    throw { status: 404, message: "Instância não encontrada" };
                req.instance = instance;
                if (next)
                    next();
            }
            catch (error) {
                return res.status((error === null || error === void 0 ? void 0 : error.status) || 500).json(error);
            }
        });
    }
}
MiddlewaresController.getInstance = function () {
    if (MiddlewaresController.middlewaresController == undefined) {
        MiddlewaresController.middlewaresController = new MiddlewaresController();
    }
    return MiddlewaresController.middlewaresController;
};
exports.default = MiddlewaresController.getInstance();
