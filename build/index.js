"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const app_1 = __importDefault(require("./app"));
const PORT = process.env.API_PORT || 3333;
app_1.default.listen(PORT, () => __awaiter(void 0, void 0, void 0, function* () {
    /* eslint-disable no-console */
    console.info(`Server ready and listening on ${PORT}`);
    /* eslint-enable no-console */
}));
// (async () => {
//   console.log("Iniciar atualização de consumo")
//   let instances = await InstanceModel.find({})
//   for (let instance of instances) {
//     let result = await updateComsumption(instance._id)
//     console.log(instance.client_tenant, result)
//   }
// })()
