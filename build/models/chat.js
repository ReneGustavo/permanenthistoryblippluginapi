"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    var desc = Object.getOwnPropertyDescriptor(m, k);
    if (!desc || ("get" in desc ? !m.__esModule : desc.writable || desc.configurable)) {
      desc = { enumerable: true, get: function() { return m[k]; } };
    }
    Object.defineProperty(o, k2, desc);
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.executeExternalChatUpdate = exports.updateChatFromBlip = exports.updateChatFromBlipByIdentity = exports.createChat = exports.ChatSchema = exports.defaultTakeChat = void 0;
const mongoose_1 = __importStar(require("mongoose"));
const utils_1 = require("../helpers/blip/utils");
const contact_1 = __importStar(require("./contact"));
const chat_1 = __importDefault(require("../classes/chat"));
const messages_1 = require("../helpers/blip/messages");
const instance_1 = require("./instance");
const migration_1 = require("./migration");
const axios_1 = __importDefault(require("axios"));
const { API_URL } = process.env;
// SCHEMA
exports.defaultTakeChat = 100;
exports.ChatSchema = new mongoose_1.Schema({
    _id: {
        type: mongoose_1.default.Schema.Types.ObjectId,
        required: true,
    },
    createdAt: {
        type: Date,
        required: true,
    },
    updatedAt: {
        type: Date,
        required: false,
    },
    instance: {
        type: mongoose_1.default.Schema.Types.ObjectId,
        required: true,
        ref: "Instance"
    },
    body: {
        type: Object,
        required: false
    },
    query: {
        type: Object,
        required: false
    },
    reference: {
        type: Object,
        required: false /*{
            type: {
                type: String,
                required: false
            },
            origin: {
                type: String,
                required: false
            },
            identity: {
                type: String,
                required: false
            }
        }*/
    }
});
const ChatModel = (0, mongoose_1.model)('Chat', exports.ChatSchema);
exports.default = ChatModel;
const createChat = ({ instance, body, query }) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const reference = (0, utils_1.getWebhookReference)(body);
        let chat = yield ChatModel.create(new chat_1.default({
            _id: new mongoose_1.default.Types.ObjectId(),
            createdAt: new Date(),
            instance: new mongoose_1.default.Types.ObjectId(instance),
            body,
            query,
            reference
        }));
    }
    catch (error) {
        throw error;
    }
});
exports.createChat = createChat;
const updateChatFromBlipByIdentity = ({ identity, lastSync, instance, token, tenantId }) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        if (!token || !tenantId) {
            let { instance: inst } = yield (0, instance_1.getInstanceById)(instance);
            if (inst) {
                token = inst.token;
                tenantId = inst.client_tenant;
            }
        }
        token = token;
        tenantId = tenantId;
        let { items } = yield (0, messages_1.getMessagesByIdentity)({ token, tenantId }, { identity, storageDate: lastSync });
        let chat = yield ChatModel.find({
            instance: new mongoose_1.default.Types.ObjectId(instance),
            "reference.identity": identity
        });
        let ids = chat.map(({ reference }) => reference === null || reference === void 0 ? void 0 : reference.id).filter((item) => item);
        yield ChatModel.insertMany(items
            .filter((item) => {
            let reference = Object.assign(Object.assign({}, (0, utils_1.getWebhookReference)(item)), { identity });
            return !reference.id || !ids.includes(reference.id);
        })
            .map((item) => {
            let reference = Object.assign(Object.assign({}, (0, utils_1.getWebhookReference)(item)), { identity });
            return new chat_1.default({
                _id: new mongoose_1.default.Types.ObjectId(),
                createdAt: new Date(),
                instance: new mongoose_1.default.Types.ObjectId(instance),
                body: item,
                reference
            });
        }));
    }
    catch (error) {
        throw error;
    }
});
exports.updateChatFromBlipByIdentity = updateChatFromBlipByIdentity;
const updateChatFromBlip = ({ instance: instanceId, page = 1, limit = exports.defaultTakeChat, where: whereAttr, migration }) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        if (!migration) {
            let { migration: m } = yield (0, migration_1.createMigration)({
                instance: instanceId,
                type: "chat",
                take: exports.defaultTakeChat
            });
            migration = m;
            if (migration.page)
                page = migration.page;
            if (migration.take)
                limit = migration.take;
        }
        let { instance } = yield (0, instance_1.getInstanceById)(instanceId);
        if (!instance)
            throw { message: "Instância não encontrada" };
        let now = migration ? migration.createdAt : new Date();
        let interval = new Date((new Date()).getTime() - (24 * 60 * 60 * 1000)); // 24 horas
        // let interval = new Date((new Date()).getTime() - (2 * 60 * 1000)) // 5 minutos
        let where = Object.assign({ instance: instance._id, $or: [
                { lastSync: undefined },
                { lastSync: { $lte: interval } }
            ] }, whereAttr);
        let totalSize = yield contact_1.default.countDocuments(where);
        if (migration && ["pending", "waiting"].includes(migration.status))
            yield (0, migration_1.updateMigration)(migration._id, { totalSize, status: "processing", updatedAt: new Date() });
        let qtd = 0;
        let finished = undefined;
        for (let i = page; i <= (migration ? page : Math.ceil(totalSize / limit)); i++) {
            let contacts = yield contact_1.default
                .find(where)
                .skip(limit * (page - 1))
                .limit(limit)
                .select("_id contact.identity lastSync");
            yield Promise.all(contacts.map((contact) => __awaiter(void 0, void 0, void 0, function* () {
                try {
                    console.log(`${++qtd} / ${totalSize}`);
                    yield (0, exports.updateChatFromBlipByIdentity)({
                        identity: contact.contact.identity,
                        token: instance.token,
                        lastSync: contact.lastSync,
                        instance: instance._id
                    });
                    yield (0, contact_1.updateContact)(contact._id, { lastSync: new Date() });
                }
                catch (_a) { }
            })));
            // if (migration)
            //     await updateMigration(migration._id, { page: ++i, updatedAt: new Date() })
        }
        // finished = migration ? (page >= Math.ceil(totalSize / limit)) : true
        finished = migration ? totalSize === 0 : true;
        yield (0, instance_1.updateInstance)(instance._id, { "lastUpdate.chat": now });
        if (migration)
            yield (0, migration_1.updateMigration)(migration._id, { status: finished ? "finished" : "waiting", updatedAt: new Date() });
        return { finished };
    }
    catch (error) {
        throw error;
    }
});
exports.updateChatFromBlip = updateChatFromBlip;
const executeExternalChatUpdate = ({ instance }) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        return axios_1.default.post(`${API_URL}/chat/update/${instance.toString()}`);
    }
    catch (error) {
        throw error;
    }
});
exports.executeExternalChatUpdate = executeExternalChatUpdate;
