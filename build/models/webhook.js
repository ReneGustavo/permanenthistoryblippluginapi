"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    var desc = Object.getOwnPropertyDescriptor(m, k);
    if (!desc || ("get" in desc ? !m.__esModule : desc.writable || desc.configurable)) {
      desc = { enumerable: true, get: function() { return m[k]; } };
    }
    Object.defineProperty(o, k2, desc);
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.onWebhook_attendanceRedirect = exports.onWebhook_message = exports.onWebhook_contact = exports.onWebhook = exports.createWebhook = exports.WebhookSchema = void 0;
const mongoose_1 = __importStar(require("mongoose"));
const webhook_1 = __importDefault(require("../classes/webhook"));
const utils_1 = require("../helpers/blip/utils");
const contact_1 = require("./contact");
const chat_1 = require("./chat");
const { WEBHOOK_SAVE } = process.env;
// SCHEMA
exports.WebhookSchema = new mongoose_1.Schema({
    _id: {
        type: mongoose_1.default.Schema.Types.ObjectId,
        required: true,
    },
    createdAt: {
        type: Date,
        required: true,
    },
    updatedAt: {
        type: Date,
        required: false,
    },
    instance: {
        type: mongoose_1.default.Schema.Types.ObjectId,
        required: true,
        ref: "Instance"
    },
    body: {
        type: Object,
        required: false
    },
    query: {
        type: Object,
        required: false
    },
    reference: {
        type: Object,
        required: false /*{
            type: {
                type: String,
                required: false
            },
            origin: {
                type: String,
                required: false
            },
            identity: {
                type: String,
                required: false
            }
        }*/
    }
});
const WebhookModel = (0, mongoose_1.model)('Webhook', exports.WebhookSchema);
exports.default = WebhookModel;
const createWebhook = ({ instance, body, query }) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const reference = (0, utils_1.getWebhookReference)(body);
        let webhook = new webhook_1.default({
            _id: new mongoose_1.default.Types.ObjectId(),
            createdAt: new Date(),
            instance: new mongoose_1.default.Types.ObjectId(instance),
            body,
            query,
            reference
        });
        if (WEBHOOK_SAVE === "true") {
            let webhookCreated = yield WebhookModel.create(webhook);
            webhook = webhookCreated;
        }
        (0, exports.onWebhook)({ instance, webhook });
    }
    catch (error) {
        throw error;
    }
});
exports.createWebhook = createWebhook;
const onWebhook = ({ instance, webhook }) => __awaiter(void 0, void 0, void 0, function* () {
    var _a, _b;
    try {
        let { reference } = webhook;
        if (reference === null || reference === void 0 ? void 0 : reference.type) {
            ((_b = (_a = {
                "contact": exports.onWebhook_contact,
                // "message": onWebhook_message,
                // "attendanceRedirect": onWebhook_attendanceRedirect
            }) === null || _a === void 0 ? void 0 : _a[reference.type]) === null || _b === void 0 ? void 0 : _b.call(_a, { instance, webhook }));
        }
    }
    catch (error) {
        throw error;
    }
});
exports.onWebhook = onWebhook;
const onWebhook_contact = ({ instance, webhook }) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        let { body } = webhook;
        yield (0, contact_1.updateOrCreateContact)({ instance, contact: body });
    }
    catch (error) {
        throw error;
    }
});
exports.onWebhook_contact = onWebhook_contact;
const onWebhook_message = ({ instance, webhook }) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        let { body } = webhook;
        yield (0, chat_1.createChat)({ instance, body });
    }
    catch (error) {
        throw error;
    }
});
exports.onWebhook_message = onWebhook_message;
const onWebhook_attendanceRedirect = ({ instance, webhook }) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        let { body } = webhook;
        yield (0, chat_1.createChat)({ instance, body });
    }
    catch (error) {
        throw error;
    }
});
exports.onWebhook_attendanceRedirect = onWebhook_attendanceRedirect;
