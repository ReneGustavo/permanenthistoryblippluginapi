"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    var desc = Object.getOwnPropertyDescriptor(m, k);
    if (!desc || ("get" in desc ? !m.__esModule : desc.writable || desc.configurable)) {
      desc = { enumerable: true, get: function() { return m[k]; } };
    }
    Object.defineProperty(o, k2, desc);
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.runMigration = exports.getMigrationToRunByInstance = exports.updateMigration = exports.createMigration = exports.MigrationSchema = void 0;
const mongoose_1 = __importStar(require("mongoose"));
const migration_1 = __importDefault(require("../classes/migration"));
const contact_1 = require("./contact");
const chat_1 = require("./chat");
const instance_1 = require("./instance");
// SCHEMA
exports.MigrationSchema = new mongoose_1.Schema({
    _id: {
        type: mongoose_1.default.Schema.Types.ObjectId,
        required: true,
    },
    createdAt: {
        type: Date,
        required: true,
    },
    updatedAt: {
        type: Date,
        required: false,
    },
    status: {
        type: String,
        required: true,
        enum: ["pending", "processing", "waiting", "finished", "canceled"]
    },
    instance: {
        type: mongoose_1.default.Schema.Types.ObjectId,
        required: true,
        ref: "Instance"
    },
    type: {
        type: String,
        required: true,
        enum: ["contact", "chat"]
    },
    filter: {
        type: Object,
        required: false
    },
    page: {
        type: Number,
        required: true,
        default: 1
    },
    take: {
        type: Number,
        required: true
    },
    totalSize: {
        type: Number,
        required: false
    },
    nextMigration: {
        type: String,
        required: false,
        enum: ["contact", "chat"]
    }
});
const MigrationModel = (0, mongoose_1.model)('Migration', exports.MigrationSchema);
exports.default = MigrationModel;
const createMigration = ({ instance, type, filter, take, nextMigration, run = false }) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        let migration = yield MigrationModel.create(new migration_1.default({
            _id: new mongoose_1.default.Types.ObjectId(),
            createdAt: new Date(),
            instance: new mongoose_1.default.Types.ObjectId(instance),
            status: "pending",
            type,
            filter,
            take,
            nextMigration
        }));
        if (run)
            setTimeout(() => (0, exports.runMigration)({ migration }), 0);
        return { migration };
    }
    catch (error) {
        throw error;
    }
});
exports.createMigration = createMigration;
const updateMigration = (_id, data) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        let migration = yield MigrationModel.findByIdAndUpdate(_id, data, { new: true });
        if (!migration)
            throw { message: "Migração não encontrada" };
        if ((data === null || data === void 0 ? void 0 : data.status) == "finished") {
            yield (0, instance_1.updateComsumption)(migration.instance);
            if (migration.nextMigration) {
                yield (0, exports.createMigration)({
                    instance: migration.instance,
                    type: migration.nextMigration,
                    filter: {},
                    take: migration.nextMigration == "chat" ? chat_1.defaultTakeChat : contact_1.defaultTakeContact,
                    run: true
                });
            }
        }
        return { migration };
    }
    catch (error) {
        throw error;
    }
});
exports.updateMigration = updateMigration;
const getMigrationToRunByInstance = ({ instance, type }) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        let where = {
            instance: new mongoose_1.default.Types.ObjectId(instance),
            status: { $in: ["pending", "waiting", "processing"] }
        };
        if (type)
            where.type = type;
        let migration = (yield MigrationModel
            .findOne(where)
            .sort("createdAt")) || undefined;
        return { migration };
    }
    catch (error) {
        throw error;
    }
});
exports.getMigrationToRunByInstance = getMigrationToRunByInstance;
const runMigration = ({ migration }) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        let { instance, type, filter, page, take } = migration;
        if (type == "contact") {
            yield (0, contact_1.updateContactsFromBlip)(Object.assign({ instance, page, take, migration }, filter));
        }
        else if (type == "chat") {
            yield (0, chat_1.updateChatFromBlip)({ instance, page, limit: take, migration });
        }
    }
    catch (error) {
        throw error;
    }
});
exports.runMigration = runMigration;
