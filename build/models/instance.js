"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    var desc = Object.getOwnPropertyDescriptor(m, k);
    if (!desc || ("get" in desc ? !m.__esModule : desc.writable || desc.configurable)) {
      desc = { enumerable: true, get: function() { return m[k]; } };
    }
    Object.defineProperty(o, k2, desc);
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.updateComsumption = exports.updateInstance = exports.createInstance = exports.getInstanceByToken = exports.getInstanceById = exports.getInstances = exports.InstanceSchema = void 0;
const mongoose_1 = __importStar(require("mongoose"));
const instance_1 = __importDefault(require("../classes/instance"));
const contact_1 = __importDefault(require("./contact"));
const chat_1 = __importDefault(require("./chat"));
const plan_1 = require("./plan");
exports.InstanceSchema = new mongoose_1.Schema({
    _id: {
        type: mongoose_1.default.Schema.Types.ObjectId,
        required: true,
    },
    created_at: {
        type: Date,
        required: true,
    },
    updated_at: {
        type: Date,
        required: false,
    },
    deleted_at: {
        type: Date,
        required: false,
    },
    status: {
        type: Number,
        required: true,
        default: 1,
    },
    execution_token: {
        type: String,
        required: true,
    },
    token: {
        type: String,
        required: true,
    },
    client_tenant: {
        type: String,
        required: true
    },
    client_access_key: {
        type: String,
        required: true
    },
    client_short_name: {
        type: String,
        required: true
    },
    plan: {
        type: mongoose_1.default.Schema.Types.ObjectId,
        required: true,
        ref: "Plan"
    },
    contacts_consumption: {
        type: Number,
        required: true
    },
    messages_consumption: {
        type: Number,
        required: true
    },
    lastUpdate: {
        type: Object,
        required: false
    }
});
const InstanceModel = (0, mongoose_1.model)('Instance', exports.InstanceSchema);
exports.default = InstanceModel;
const getInstances = (where = {}) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        let instances = yield InstanceModel
            .find(Object.assign(Object.assign({}, where), { deletedAt: undefined, deleted_at: undefined }))
            .populate({ path: "plan", select: "_id name contacts_limit messages_limit" })
            .lean();
        return { instances };
    }
    catch (error) {
        throw error;
    }
});
exports.getInstances = getInstances;
const getInstanceById = (_id) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        let instance = yield InstanceModel
            .findOne({ _id: new mongoose_1.default.Types.ObjectId(_id), deletedAt: undefined, deleted_at: undefined })
            .populate({ path: "plan", select: "_id name contacts_limit messages_limit" })
            .lean();
        return { instance };
    }
    catch (error) {
        throw error;
    }
});
exports.getInstanceById = getInstanceById;
const getInstanceByToken = (token) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        let instance = yield InstanceModel
            .findOne({ token })
            .populate({ path: "plan", select: "_id name contacts_limit messages_limit" })
            .lean();
        return { instance };
    }
    catch (error) {
        throw error;
    }
});
exports.getInstanceByToken = getInstanceByToken;
const createInstance = ({ execution_token, token, client_tenant, client_access_key, client_short_name, plan }) => __awaiter(void 0, void 0, void 0, function* () {
    var _a;
    try {
        let _id = new mongoose_1.default.Types.ObjectId();
        if (!((_a = (yield (0, plan_1.getPlanById)(plan))) === null || _a === void 0 ? void 0 : _a.plan))
            throw { message: "Plano não existente" };
        let instance = yield InstanceModel.create(new instance_1.default({
            _id,
            created_at: new Date(),
            status: 1,
            execution_token,
            token,
            client_tenant,
            client_access_key,
            client_short_name,
            plan,
            contacts_consumption: 0,
            messages_consumption: 0
        }));
        return instance;
    }
    catch (error) {
        throw error;
    }
});
exports.createInstance = createInstance;
const updateInstance = (_id, data) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        let instance = yield InstanceModel.findByIdAndUpdate(_id, Object.assign({ updated_at: new Date() }, data), { new: true });
        return instance;
    }
    catch (error) {
        throw error;
    }
});
exports.updateInstance = updateInstance;
const updateComsumption = (instance) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        let inst = yield InstanceModel.findById(instance);
        if (!inst)
            throw { message: "Instancâ não encontrada" };
        let where = {
            client_tenant: inst.client_tenant
        };
        let instances = yield InstanceModel.find(Object.assign({ deletedAt: undefined, status: 1 }, where));
        let whereCount = {
            instance: {
                $in: instances.map(({ _id }) => _id)
            }
        };
        let [contacts_consumption, messages_consumption] = yield Promise.all([
            contact_1.default.countDocuments(whereCount),
            chat_1.default.countDocuments(whereCount)
        ]);
        let data = { contacts_consumption, messages_consumption };
        yield InstanceModel.findByIdAndUpdate(instance, data);
        return data;
    }
    catch (error) {
        throw error;
    }
});
exports.updateComsumption = updateComsumption;
