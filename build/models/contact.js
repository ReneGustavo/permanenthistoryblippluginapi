"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    var desc = Object.getOwnPropertyDescriptor(m, k);
    if (!desc || ("get" in desc ? !m.__esModule : desc.writable || desc.configurable)) {
      desc = { enumerable: true, get: function() { return m[k]; } };
    }
    Object.defineProperty(o, k2, desc);
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.executeExternalContactUpdate = exports.updateContactsFromBlip = exports.updateContact = exports.updateOrCreateContact = exports.createContact = exports.getContactByIdentity = exports.listContacts = exports.ContactSchema = exports.defaultTakeContact = void 0;
const mongoose_1 = __importStar(require("mongoose"));
const contact_1 = __importDefault(require("../classes/contact"));
const contacts_1 = require("../helpers/blip/contacts");
const instance_1 = require("./instance");
const moment_1 = __importDefault(require("moment"));
const migration_1 = require("./migration");
const axios_1 = __importDefault(require("axios"));
const { API_URL } = process.env;
exports.defaultTakeContact = 100;
// SCHEMA
exports.ContactSchema = new mongoose_1.Schema({
    _id: {
        type: mongoose_1.default.Schema.Types.ObjectId,
        required: true,
    },
    createdAt: {
        type: Date,
        required: true,
    },
    updatedAt: {
        type: Date,
        required: false,
    },
    instance: {
        type: mongoose_1.default.Schema.Types.ObjectId,
        required: true
    },
    contact: {
        type: Object,
        required: false
    },
    lastSync: {
        type: Date,
        required: false
    }
});
const ContactModel = (0, mongoose_1.model)('Contact', exports.ContactSchema);
exports.default = ContactModel;
const listContacts = ({ instance, search }) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        // let aggregate = await WebhookModel.aggregate([
        //     {
        //         $match: {
        //             instance: new mongoose.Types.ObjectId(instance),
        //             "reference.type": "message"
        //         },
        //     },
        //     {
        //         $sort: {
        //             createdAt: -1,
        //         },
        //     },
        //     {
        //         $group: {
        //             _id: "$reference.identity",
        //         },
        //     },
        //     {
        //         $lookup: {
        //             from: "contacts",
        //             localField: "_id",
        //             foreignField: "contact.identity",
        //             as: "contact",
        //         },
        //     },
        //     {
        //         $unwind: {
        //             path: "$contact",
        //             includeArrayIndex: "string",
        //             preserveNullAndEmptyArrays: true,
        //         },
        //     },
        //     {
        //         $project: {
        //             _id: 0,
        //             identity: "$_id",
        //             contact: "$contact.contact",
        //         },
        //     },
        // ])
        let where = {
            instance: new mongoose_1.default.Types.ObjectId(instance)
        };
        if (search)
            where.$or = [
                { "contact.name": { $regex: `.*${search}.*`, $options: "i" } },
                { "contact.phoneNumber": { $regex: `.*${search}.*`, $options: "i" } },
                { "contact.identity": { $regex: `.*${search}.*`, $options: "i" } },
                { "contact.document": { $regex: `.*${search}.*`, $options: "i" } },
                { "contact.gender": { $regex: `.*${search}.*`, $options: "i" } },
                { "contact.city": { $regex: `.*${search}.*`, $options: "i" } },
            ];
        console.log(JSON.stringify(where));
        let contacts = yield ContactModel.find(where);
        return { contacts };
    }
    catch (error) {
        throw error;
    }
});
exports.listContacts = listContacts;
const getContactByIdentity = ({ instance, identity }) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        let contact = yield ContactModel.findOne({ instance, "contact.identity": identity, deletedAt: undefined });
        return { contact };
    }
    catch (error) {
        throw error;
    }
});
exports.getContactByIdentity = getContactByIdentity;
const createContact = ({ instance, contact }) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        let { identity } = contact;
        if (!identity)
            throw { message: "Identity não informado" };
        let cont = new contact_1.default({
            _id: new mongoose_1.default.Types.ObjectId(),
            createdAt: new Date(),
            instance: new mongoose_1.default.Types.ObjectId(instance),
            contact
        });
        let contactCreated = yield ContactModel.create(cont);
        return { contact: contactCreated };
    }
    catch (error) {
        throw error;
    }
});
exports.createContact = createContact;
const updateOrCreateContact = ({ instance, contact }) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        let { identity } = contact;
        let newContact;
        let contactUpdated = yield ContactModel.findOneAndUpdate({ "contact.identity": identity, instance }, { contact, updatedAt: new Date() }, { new: true });
        if (contactUpdated)
            newContact = contactUpdated;
        else {
            let { contact: contactCreated } = yield (0, exports.createContact)({ instance, contact });
            newContact = contactCreated;
        }
        return { contact: newContact };
    }
    catch (error) {
        throw error;
    }
});
exports.updateOrCreateContact = updateOrCreateContact;
const updateContact = (_id, data) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        let contact = yield ContactModel.findByIdAndUpdate(_id, data, { new: true });
        return { contact };
    }
    catch (error) {
        throw error;
    }
});
exports.updateContact = updateContact;
const updateContactsFromBlip = ({ instance: instanceId, lastUpdate, page = 1, take = exports.defaultTakeContact, migration }) => __awaiter(void 0, void 0, void 0, function* () {
    let finished = undefined;
    try {
        console.log("{ updateContactsFromBlip }", { instanceId, lastUpdate, page, take, migration });
        let now = migration ? migration.createdAt : new Date();
        if (migration) {
            if (migration.page)
                page = migration.page;
            if (migration.take)
                take = migration.take;
        }
        else {
            let { migration: m } = yield (0, migration_1.createMigration)({
                instance: instanceId,
                type: "contact",
                take: exports.defaultTakeContact
            });
            migration = m;
        }
        let { instance } = yield (0, instance_1.getInstanceById)(instanceId);
        if (!instance)
            throw { message: "Instância não encontrada" };
        let skip = (page - 1) * take;
        let result = { quantity: 0 };
        if (migration && ["pending", "waiting"].includes(migration.status))
            yield (0, migration_1.updateMigration)(migration._id, { status: "processing", updatedAt: new Date() });
        let { items: contacts, total: totalSize } = yield (0, contacts_1.getContacts)({ token: instance.token, tenantId: instance.client_tenant }, {
            skip,
            take,
            filter: lastUpdate ? `(lastmessagedate%20ge%20datetimeoffset'${(0, moment_1.default)(lastUpdate).toISOString()}')` : undefined //`(lastmessagedate%20le%20datetimeoffset'${moment().toISOString()}')`
        });
        console.log({ page, take, totalSize });
        if (migration && migration.totalSize != totalSize)
            yield (0, migration_1.updateMigration)(migration._id, { totalSize, updatedAt: new Date() });
        result.quantity = totalSize;
        yield Promise.all(contacts.map((contact) => (0, exports.updateOrCreateContact)({ instance: instanceId, contact })));
        console.log(`${totalSize > (take * page) ? take * page : totalSize} / ${totalSize}`);
        if (totalSize > (take * page)) {
            skip = take * page;
            page++;
            finished = false;
            result = Object.assign(Object.assign({}, result), { page, finished });
        }
        else {
            finished = true;
            result = Object.assign(Object.assign({}, result), { finished });
        }
        if (migration) {
            yield (0, migration_1.updateMigration)(migration._id, { status: finished ? "finished" : "waiting", page, updatedAt: new Date() });
        }
        if (finished)
            yield (0, instance_1.updateInstance)(instance._id, { "lastUpdate.contact": now });
        // console.log(result)
        return result;
    }
    catch (error) {
        console.error(error);
        // if (finished === undefined || finished === false)
        //     executeExternalContactUpdate({ instance: new mongoose.Types.ObjectId(instanceId) })
        throw error;
    }
});
exports.updateContactsFromBlip = updateContactsFromBlip;
const executeExternalContactUpdate = ({ instance }) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        return axios_1.default.post(`${API_URL}/contacts/update/${instance.toString()}`);
    }
    catch (error) {
        throw error;
    }
});
exports.executeExternalContactUpdate = executeExternalContactUpdate;
