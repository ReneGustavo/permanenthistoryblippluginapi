"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class Chat {
    constructor(chat) {
        this._id = chat._id;
        this.createdAt = chat.createdAt;
        this.updatedAt = chat.updatedAt;
        this.instance = chat.instance;
        this.body = chat.body;
        this.query = chat.query;
        this.reference = chat.reference;
    }
}
exports.default = Chat;
