"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class Instance {
    constructor(instance) {
        this._id = instance._id;
        this.created_at = instance.created_at;
        this.updated_at = instance.updated_at;
        this.deleted_at = instance.deleted_at;
        this.status = instance.status;
        this.execution_token = instance.execution_token;
        this.token = instance.token;
        this.client_tenant = instance.client_tenant;
        this.client_access_key = instance.client_access_key;
        this.client_short_name = instance.client_short_name;
        this.plan = instance.plan;
        this.contacts_consumption = instance.contacts_consumption;
        this.messages_consumption = instance.messages_consumption;
        this.lastUpdate = instance.lastUpdate;
        this.migration = instance.migration;
    }
}
exports.default = Instance;
