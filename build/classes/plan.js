"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class Plan {
    constructor(plan) {
        this._id = plan._id;
        this.createdAt = plan.createdAt;
        this.updatedAt = plan.updatedAt;
        this.deletedAt = plan.deletedAt;
        this.status = plan.status;
        this.name = plan.name;
        this.contacts_limit = plan.contacts_limit;
        this.messages_limit = plan.messages_limit;
    }
}
exports.default = Plan;
