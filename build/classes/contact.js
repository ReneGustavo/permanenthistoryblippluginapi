"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class Contact {
    constructor(contact) {
        this._id = contact._id;
        this.createdAt = contact.createdAt;
        this.updatedAt = contact.updatedAt;
        this.instance = contact.instance;
        this.contact = contact.contact;
        this.lastSync = contact.lastSync;
    }
}
exports.default = Contact;
