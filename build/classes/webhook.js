"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class Webhook {
    constructor(webhook) {
        this._id = webhook._id;
        this.createdAt = webhook.createdAt;
        this.updatedAt = webhook.updatedAt;
        this.instance = webhook.instance;
        this.body = webhook.body;
        this.query = webhook.query;
        this.reference = webhook.reference;
    }
}
exports.default = Webhook;
