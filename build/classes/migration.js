"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class Migration {
    constructor(migration) {
        this._id = migration._id;
        this.createdAt = migration.createdAt;
        this.updatedAt = migration.updatedAt;
        this.instance = migration.instance;
        this.status = migration.status;
        this.type = migration.type;
        this.filter = migration.filter;
        this.page = migration.page;
        this.take = migration.take;
        this.totalSize = migration.totalSize;
        this.nextMigration = migration.nextMigration;
    }
}
exports.default = Migration;
