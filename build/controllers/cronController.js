"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const instance_1 = require("../models/instance");
const contact_1 = require("../models/contact");
const chat_1 = require("../models/chat");
const { API_URL } = process.env;
class CRONController {
    routineUpdatedChats(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                let { instances = [] } = yield (0, instance_1.getInstances)();
                yield Promise.all(instances.map((instance) => (0, chat_1.executeExternalChatUpdate)({ instance: instance._id })));
                res === null || res === void 0 ? void 0 : res.status(200).json({
                    success: true
                });
            }
            catch (error) {
                res === null || res === void 0 ? void 0 : res.status((error === null || error === void 0 ? void 0 : error.status) || 500).json(error);
            }
        });
    }
    routineUpdatedContacts(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                let { instances = [] } = yield (0, instance_1.getInstances)();
                yield Promise.all(instances.map((instance) => (0, contact_1.executeExternalContactUpdate)({ instance: instance._id })));
                res === null || res === void 0 ? void 0 : res.status(200).json({
                    success: true
                });
            }
            catch (error) {
                res === null || res === void 0 ? void 0 : res.status((error === null || error === void 0 ? void 0 : error.status) || 500).json(error);
            }
        });
    }
}
CRONController.getInstance = function () {
    if (CRONController.webhookController == undefined) {
        CRONController.webhookController = new CRONController();
    }
    return CRONController.webhookController;
};
exports.default = CRONController.getInstance();
