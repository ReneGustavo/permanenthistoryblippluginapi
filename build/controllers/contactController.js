"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const contact_1 = require("../models/contact");
const migration_1 = require("../models/migration");
const instance_1 = require("../models/instance");
const util_1 = require("../helpers/util");
class WebhookController {
    list(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                let { instance, query } = req;
                let { search } = query;
                let result = yield (0, contact_1.listContacts)({
                    instance: instance._id,
                    search
                });
                res === null || res === void 0 ? void 0 : res.status(200).json(Object.assign(Object.assign({}, result), { success: true }));
            }
            catch (error) {
                console.error(error);
                res === null || res === void 0 ? void 0 : res.status((error === null || error === void 0 ? void 0 : error.status) || 500).json(error);
            }
        });
    }
    update(req, res) {
        var _a, _b;
        return __awaiter(this, void 0, void 0, function* () {
            try {
                let { instance: instanceId } = req.params;
                let { force = false } = req.query;
                force = [true, "true"].includes(force);
                let { instance } = yield (0, instance_1.getInstanceById)(instanceId);
                if (!instance)
                    throw { message: "Instância não encontrada" };
                let finished = true;
                do {
                    let { migration } = yield (0, migration_1.getMigrationToRunByInstance)({ instance: instanceId, type: "contact" });
                    console.log("migration", migration);
                    let lastUpdate = (_a = instance === null || instance === void 0 ? void 0 : instance.lastUpdate) === null || _a === void 0 ? void 0 : _a.contact;
                    if ((migration === null || migration === void 0 ? void 0 : migration.status) == "processing" && (!migration.updatedAt || (0, util_1.calculateTimeDifferenceInSeconds)(migration.updatedAt, new Date()) < 15))
                        throw { success: false, message: "No momento há uma migração em andamento" };
                    if (!migration && !force) {
                        if (lastUpdate && (0, util_1.calculateTimeDifferenceInMinutes)(lastUpdate, new Date()) < (60 * 1))
                            throw { success: false, message: "Fora do período de atualização" };
                    }
                    let { finished: f } = yield (0, contact_1.updateContactsFromBlip)({
                        instance: instance._id,
                        lastUpdate: (_b = instance === null || instance === void 0 ? void 0 : instance.lastUpdate) === null || _b === void 0 ? void 0 : _b.contact,
                        migration
                    });
                    finished = f;
                } while (finished === false);
                res === null || res === void 0 ? void 0 : res.status(200).json({
                    success: true
                });
            }
            catch (error) {
                console.error(error);
                res === null || res === void 0 ? void 0 : res.status((error === null || error === void 0 ? void 0 : error.status) || 500).json(error);
            }
        });
    }
}
WebhookController.getInstance = function () {
    if (WebhookController.webhookController == undefined) {
        WebhookController.webhookController = new WebhookController();
    }
    return WebhookController.webhookController;
};
exports.default = WebhookController.getInstance();
