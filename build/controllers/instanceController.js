'use strict';
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    var desc = Object.getOwnPropertyDescriptor(m, k);
    if (!desc || ("get" in desc ? !m.__esModule : desc.writable || desc.configurable)) {
      desc = { enumerable: true, get: function() { return m[k]; } };
    }
    Object.defineProperty(o, k2, desc);
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
require("dotenv/config");
const instance_1 = __importStar(require("../models/instance"));
const blip_1 = require("../helpers/blip");
const contact_1 = require("../models/contact");
const migration_1 = __importStar(require("../models/migration"));
const mongoose_1 = __importDefault(require("mongoose"));
const { API_URL_PROD } = process.env;
class InstanceController {
    get(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                // console.log(req.headers)
                let { language } = req.headers;
                let Authorization = req.headers.Authorization || req.headers.authorization;
                let tenantId = req.headers.tenantId || req.headers.tenantid;
                if (!Authorization)
                    throw { status: 404, message: "Authorization não informado" };
                if (!tenantId)
                    throw { status: 404, message: "tenantId não informado" };
                let instance = yield instance_1.default
                    .findOne({ client_tenant: tenantId, token: Authorization, deleted_at: undefined })
                    .populate({ path: "plan", select: "_id name contacts_limit messages_limit" })
                    .lean();
                if (!instance || (instance === null || instance === void 0 ? void 0 : instance.status) == 0)
                    throw { status: 404, message: "Instância não encontrada", notFound: true };
                if ((instance === null || instance === void 0 ? void 0 : instance.status) == 2)
                    throw { status: 401, message: "Instância sem permissão" };
                if (instance.token != Authorization)
                    throw {
                        status: 200,
                        success: false,
                        messageHTML: `Esta instância só pode ser acessada via <b>${instance.client_short_name}</b>`,
                        message: `Esta instância só pode ser acessada via ${instance.client_short_name}`,
                        otherBot: true,
                        bot: instance.client_short_name
                    };
                instance.webhook = `${API_URL_PROD}/webhook/${instance._id}`;
                instance.migration = yield migration_1.default
                    .findOne({
                    instance: instance._id,
                    status: { $in: ["waiting", "processing"] }
                });
                res.status(200).json({
                    success: true,
                    instance
                });
            }
            catch (error) {
                console.error(error);
                res.status((error === null || error === void 0 ? void 0 : error.status) || 500).json(error);
            }
        });
    }
    post(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                // console.log(req.headers)
                let { language } = req.headers;
                let Authorization = req.headers.Authorization || req.headers.authorization;
                let tenantId = req.headers.tenantId || req.headers.tenantid;
                let { plan } = req.body;
                // let {
                //     execution_token,
                //     service = "middleware_for_blip"
                // } = req.body
                let execution_token = Authorization;
                if (!Authorization)
                    throw { status: 404, message: "Authorization não informado" };
                if (!tenantId)
                    throw { status: 404, message: "tenantId não informado" };
                if (!plan)
                    throw { stats: 404, message: "Plano não informado" };
                let check = yield (0, blip_1.checkConnectivity)({ token: Authorization, tenantId });
                if (!(check === null || check === void 0 ? void 0 : check.status) || (check === null || check === void 0 ? void 0 : check.status) != "success")
                    throw { status: 401, message: "Instância inválida" };
                check = yield (0, blip_1.checkConnectivity)({ token: execution_token, tenantId });
                if (!(check === null || check === void 0 ? void 0 : check.status) || (check === null || check === void 0 ? void 0 : check.status) != "success")
                    throw { status: 401, message: "Roteador ou bot inválido" };
                let { shortName, accessKey } = (0, blip_1.authorizationDecode)(Authorization);
                let instanceCreated = yield (0, instance_1.createInstance)({
                    execution_token,
                    token: Authorization,
                    client_tenant: tenantId,
                    client_access_key: accessKey,
                    client_short_name: shortName,
                    plan: new mongoose_1.default.Types.ObjectId(plan)
                });
                yield (0, blip_1.setWebhook)({ token: Authorization, tenantId }, true, `${API_URL_PROD}/webhook/${instanceCreated._id}`);
                let { migration } = yield (0, migration_1.createMigration)({
                    instance: instanceCreated._id,
                    type: "contact",
                    take: contact_1.defaultTakeContact,
                    nextMigration: "chat"
                });
                let { instance } = yield (0, instance_1.getInstanceById)(instanceCreated._id);
                if (!instance || (instance === null || instance === void 0 ? void 0 : instance.status) == 0)
                    throw { status: 404, message: "Instância não encontrada", notFound: true };
                if ((instance === null || instance === void 0 ? void 0 : instance.status) == 2)
                    throw { status: 401, message: "Instância sem permissão" };
                instance.migration = migration;
                res.status(200).json({
                    success: true,
                    data: instance
                });
            }
            catch (error) {
                console.error(error);
                res.status((error === null || error === void 0 ? void 0 : error.status) || 500).json(error);
            }
        });
    }
    put(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                let { language } = req.headers;
                let Authorization = req.headers.Authorization || req.headers.authorization;
                let { instance } = req.decoded;
                let { execution_token } = req.body;
                const check = yield (0, blip_1.checkConnectivity)(Authorization);
                if (!(check === null || check === void 0 ? void 0 : check.status) || (check === null || check === void 0 ? void 0 : check.status) != "success")
                    throw { status: 401, message: "Instância inválida" };
                if (!instance || (instance === null || instance === void 0 ? void 0 : instance.status) == 0)
                    throw { status: 404, message: "Instância não encontrada", notFound: true };
                if ((instance === null || instance === void 0 ? void 0 : instance.status) == 2)
                    throw { status: 401, message: "Instância sem permissão" };
                instance = yield (0, instance_1.updateInstance)(instance._id, {
                    execution_token
                });
                res.status(200).json({
                    success: true,
                    data: instance
                });
            }
            catch (error) {
                console.error(error);
                res.status((error === null || error === void 0 ? void 0 : error.status) || 500).json(error);
            }
        });
    }
    updateWebhook(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                let Authorization = req.headers.Authorization || req.headers.authorization;
                let { instance } = req;
                yield (0, blip_1.setWebhook)({
                    token: Authorization,
                    tenantId: instance.client_tenant
                }, true, `${API_URL_PROD}/webhook/${instance._id}`);
                res.status(200).json({
                    success: true
                });
            }
            catch (error) {
                console.error(error);
                res.status((error === null || error === void 0 ? void 0 : error.status) || 500).json(error);
            }
        });
    }
}
InstanceController.getInstance = function () {
    if (InstanceController.instanceController == undefined) {
        InstanceController.instanceController = new InstanceController();
    }
    return InstanceController.instanceController;
};
exports.default = InstanceController.getInstance();
