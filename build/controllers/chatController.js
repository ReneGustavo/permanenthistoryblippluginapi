"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    var desc = Object.getOwnPropertyDescriptor(m, k);
    if (!desc || ("get" in desc ? !m.__esModule : desc.writable || desc.configurable)) {
      desc = { enumerable: true, get: function() { return m[k]; } };
    }
    Object.defineProperty(o, k2, desc);
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const contact_1 = require("../models/contact");
const mongoose_1 = __importDefault(require("mongoose"));
const chat_1 = __importStar(require("../models/chat"));
const migration_1 = require("../models/migration");
const instance_1 = require("../models/instance");
const util_1 = require("../helpers/util");
class ChatController {
    get(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                let { identity } = req.params;
                let { instance } = req;
                let { contact } = (yield (0, contact_1.getContactByIdentity)({ instance: instance._id, identity })) || {};
                if (contact)
                    yield (0, chat_1.updateChatFromBlipByIdentity)({
                        identity,
                        lastSync: contact.lastSync,
                        instance: instance._id,
                        token: instance.token,
                        tenantId: instance.client_tenant
                    });
                let where = {
                    instance: new mongoose_1.default.Types.ObjectId(instance._id),
                    "reference.type": { $in: ["message", "attendanceRedirect"] },
                    "reference.identity": identity
                };
                console.log(where);
                let chat = yield chat_1.default
                    .find(where)
                    .select("-instance")
                    .sort("reference.datetime");
                res === null || res === void 0 ? void 0 : res.status(200).json({
                    chat,
                    contact: contact === null || contact === void 0 ? void 0 : contact.contact,
                    success: true
                });
            }
            catch (error) {
                console.error(error);
                res === null || res === void 0 ? void 0 : res.status((error === null || error === void 0 ? void 0 : error.status) || 500).json(error);
            }
        });
    }
    update(req, res) {
        var _a, _b;
        return __awaiter(this, void 0, void 0, function* () {
            try {
                let { instance: instanceId } = req.params;
                let { force = false } = req.query;
                force = [true, "true"].includes(force);
                let { instance } = yield (0, instance_1.getInstanceById)(instanceId);
                if (!instance)
                    throw { message: "Instância não encontrada" };
                let finished = true;
                do {
                    let { migration } = yield (0, migration_1.getMigrationToRunByInstance)({ instance: instanceId, type: "chat" });
                    let lastUpdate = (_a = instance === null || instance === void 0 ? void 0 : instance.lastUpdate) === null || _a === void 0 ? void 0 : _a.chat;
                    if ((migration === null || migration === void 0 ? void 0 : migration.status) == "processing" && (!migration.updatedAt || (0, util_1.calculateTimeDifferenceInSeconds)(migration.updatedAt, new Date()) < 15))
                        throw { success: false, message: "No momento há uma migração em andamento" };
                    if (!migration && !force) {
                        if (lastUpdate && (0, util_1.calculateTimeDifferenceInMinutes)(lastUpdate, new Date()) < (60 * 24))
                            throw { success: false, message: "Fora do período de atualização" };
                    }
                    let { finished: f } = yield (0, chat_1.updateChatFromBlip)({
                        instance: instance._id,
                        migration
                    });
                    finished = f;
                } while (finished === false);
                res === null || res === void 0 ? void 0 : res.status(200).json({
                    success: true
                });
            }
            catch (error) {
                console.error((_b = error === null || error === void 0 ? void 0 : error.response) === null || _b === void 0 ? void 0 : _b.data);
                res === null || res === void 0 ? void 0 : res.status((error === null || error === void 0 ? void 0 : error.status) || 500).json(error);
            }
        });
    }
}
ChatController.getInstance = function () {
    if (ChatController.chatController == undefined) {
        ChatController.chatController = new ChatController();
    }
    return ChatController.chatController;
};
exports.default = ChatController.getInstance();
